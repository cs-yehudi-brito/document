﻿angular.module('SGA', ['ngResource'
                         , 'nya.bootstrap.select'
                         , 'nemLogging'
                         , 'ui-leaflet'
                         , 'ngTagsInput'
                         , 'ngFileUpload'
                         , 'ngAnimate'
                         , 'ui.bootstrap'
                         , 'ui.router'
                         , 'angularMoment'
                         , 'ngCookies'
                         , 'ncy-angular-breadcrumb'
                         , 'ngMaterial'
                         , 'ngAnimate'
                         , 'ngAria'
                         , 'cgPrompt'
                         , 'bootstrapLightbox'
                         , 'angularBootstrapNavTree'
                         
]);

angular.module('SGA').config(['$breadcrumbProvider', function ($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: '<ol class="breadcrumb">' +
            '<li><i class="fa fa-folder-open-o"></i> Sistema de Gestão Ambiental e Territorial</li>' +
           '<li ng-repeat="step in steps" ng-switch="$last || !!step.abstract" ng-class="{active: $last}">' +
            '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a>' +
            '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>' +
            '<span class="divider" ng-hide="$last"></span>' +
            '</li>' +
            '</ol>'
    });
}]);

angular.module('SGA').config(['$urlRouterProvider', '$locationProvider', '$stateProvider', 'nyaBsConfigProvider', function ($urlRouterProvider, $locationProvider, $stateProvider, nyaBsConfigProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state("main", {
            abstract: true,
            views: {
                "menu": {
                    templateUrl: 'assets/templates/menu/menu.html',
                    controller: 'MenuController as ctrl',
                },
                "header": {
                    templateUrl: 'assets/templates/menu/topHeader.html',
                    controller: 'LoginController as ctrl',
                },
                "breadcrumbs": {
                    templateUrl: 'assets/templates/menu/breadcrumbs.html',
                }
            }
        })
        .state("home", {
            parent: 'main',
            url: '/home',
            ncyBreadcrumb: { label: 'Home' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/Home.html',
                    controller: 'LoginController as ctrl',
                }
            }
        })
       .state("login", {
           parent: 'main',
           url: '/login',
           ncyBreadcrumb: { label: 'Login' },
           data: { isPublic: true },
           views: {
               "content@": {
                   templateUrl: 'assets/templates/Login.html',
                   controller: 'LoginController as ctrl',
               }
           }
       })
        .state("account", {
            parent: 'main',
            url: '/register',
            ncyBreadcrumb: { label: 'Lista de Usuario' },

            views: {
                "content@": {
                    templateUrl: 'assets/templates/account/search.html',
                    controller: 'AccountSearchController as ctrl',
                }
            }
        })
        .state('account.search', {
            url: '/search',
            ncyBreadcrumb: { label: 'Lista de Usuario' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/account/search.html',
                    controller: 'AccountSearchController as ctrl',
                }
            }
        })
            .state('account.add', {
                url: '/add',
                ncyBreadcrumb: { label: 'Cadastro de Usuario' },
                views: {
                    "content@": {
                        templateUrl: 'assets/templates/account/add.html',
                        controller: 'AccountController as ctrl',
                    }
                }
            })
         .state("account.id", {
             url: '/:email',
             ncyBreadcrumb: { label: '{{ctrl.user.person.name}}' },
             views: {
                 "content@": {
                     templateUrl: 'assets/templates/account/edit.html',
                     controller: 'AccountController as ctrl',
                 }
             }

         })
    .state("incidents",
        {
            parent: 'main',
            url: '/incidents',
            ncyBreadcrumb: { label: 'Incidentes' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/search.html',
                    data: { module: 'incidents' },
                    controller: 'IncidentSearchController as ctrl'
                }
            }
        })
    .state("incidents.search",
        {
            url: '/search',
            ncyBreadcrumb: { label: 'Pesquisar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/search.html',
                    controller: 'IncidentSearchController as ctrl'
                }
            }
        })
    .state("incidents.id",
        {
            url: '/edit/:id',
            ncyBreadcrumb: { label: '{{ctrl.code}}' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/edit.html',
                    controller: 'IncidentController as ctrl'
                }
            }
        })
    .state("incidents.add",
        {
            url: '/add',
            ncyBreadcrumb: { label: 'Adicionar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/edit.html',
                    controller: 'IncidentController as ctrl'
                }
            }
        })
        .state("highwayaccessroads",
        {
            parent: 'main',
            params: { module: "ACR" },
            url: '/highwayaccessroads',
            ncyBreadcrumb: { label: 'Acesso' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/search.html',
                    data: { module: 'occupation' },
                    controller: 'HighWayAccessRoadSearchController as ctrl'
                }
            }
        })
    .state("highwayaccessroads.search",
        {
            url: '/search',
            ncyBreadcrumb: { label: 'Pesquisar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/search.html',
                    controller: 'HighWayAccessRoadSearchController as ctrl'
                }
            }
        }
    )
    .state("highwayaccessroads.id",
        {
            url: '/edit/:id',
            ncyBreadcrumb: { label: '{{ctrl.code}}' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/edit.html',
                    controller: 'HighWayAccessRoadController as ctrl'
                }
            }
        })
    .state("highwayaccessroads.add",
        {
            url: '/add',
            ncyBreadcrumb: { label: 'Adicionar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/edit.html',
                    controller: 'HighWayAccessRoadController as ctrl'
                }
            }

        })
     .state("highwayaccessroads.report",
        {
            url: '/reports',
            ncyBreadcrumb: { label: 'Relatórios' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/report/report.html',
                    controller: 'ReportController as ctrl'
                }
            }
        })
      .state("roadsideoccupations",
        {
            parent: 'main',
            url: '/roadsideoccupations',
            params: { module: "OCR" },
            ncyBreadcrumb: { label: 'Ocupação' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/search.html',
                    controller: 'RoadsideOccupationSearchController as ctrl'
                }
            }
        })
    .state("roadsideoccupations.search",
        {
            url: '/search',
            ncyBreadcrumb: { label: 'Pesquisar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/search.html',
                    controller: 'RoadsideOccupationSearchController as ctrl'
                }
            }
        })
    .state("roadsideoccupations.id",
        {
            url: '/edit/:id',
            ncyBreadcrumb: { label: '{{ctrl.code}}' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/edit.html',
                    controller: 'RoadsideOccupationController as ctrl'
                }
            },
        })
    .state("roadsideoccupations.add",
        {
            url: '/add',
            ncyBreadcrumb: { label: 'Adicionar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/edit.html',
                    controller: 'RoadsideOccupationController as ctrl'
                }
            }
        })

         .state("roadsideoccupations.report",
        {
            url: '/reports',
            ncyBreadcrumb: { label: 'Relatórios' },
            views: {
                "content@": {

                    templateUrl: 'assets/templates/report/report.html',
                    controller: 'ReportController as ctrl'
                }
            }
        })

        .state('roadsideoccupations.map', {
            url: '/map',
            params: { entityName: 'roadsideoccupation' },
            ncyBreadcrumb: { label: 'Mapa' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/mapa/mapa.html',
                    controller: 'MapController as ctrl'
                }
            }
        })

    .state('highways', {

        parent: 'main',
        url: '/highways',
        ncyBreadcrumb: { label: 'Rodovia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/search.html',
                controller: 'HighwaySearchController as ctrl'
            }
        }
    })
    .state('highways.search', {
        url: '/search',
        ncyBreadcrumb: { label: 'Pesquisar' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/search.html',
                controller: 'HighwaySearchController as ctrl'
            }
        }
    })
    .state('highways.id', {
        url: '/edit/:id',
        ncyBreadcrumb: { label: '{{ctrl.highway.name}}' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/edit.html',
                controller: 'HighwayController as ctrl'
            }
        }
    })
    .state('highways.add', {
        url: '/add',
        ncyBreadcrumb: { label: 'Cadastro de Rodovia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/add.html',
                controller: 'HighwayController as ctrl'
            }
        }
    })
    .state('report', {
        parent: 'main',
        url: '/report',
        ncyBreadcrumb: { label: 'Relatórios' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/report/relatorio.html',
                controller: 'ReportController as ctrl'
            }
        }
    })
    .state('report.relatorio', {
        url: '/relatorio',
        ncyBreadcrumb: { label: 'Pesquisar' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/report/relatorio.html',
                controller: 'ReportController as ctrl'
            }
        }
    })

        .state('forestry',{
            parent: 'main',
            url: '/forestry',
            ncyBreadcrumb: { label: 'Eng. Florestal' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/forestry/tree.html',
                    controller: 'ForestryController as ctrl'
                }
            }
        })
     
         .state('forestry.parcel', {
         })

        .state('forestry.parcel.id', {
            url: '/:id',
            ncyBreadcrumb: { label: 'Parcela {{ ctrl.Parcel.Name }}' },
            views: {
                "details@forestry": {
                    templateUrl: 'assets/templates/forestry/parcel.html',
                    controller: 'ForestryParcelController as ctrl'
                }
            }
        })


        .state('app', {
        parent: 'main',
        url: '/app',
        ncyBreadcrumb: { label: 'App Rodovia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highwayapp/highwayapp.html',
                controller: 'HighwayAppController as ctrl'
            }
        }
    })
    .state('app.accessroad', {
        url: '/accessroad',
        ncyBreadcrumb: { label: 'App de Acessos a Rovodia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highwayapp/highwayapp.html',
                controller: 'AppAccessRoadController as ctrl'
            }
        }
    })
    .state('app.accessroad.accessroadid', {
        url: '/:id',
        ncyBreadcrumb: { label: 'App de Acessos a Rovodia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highwayapp/appinformation.html',
                controller: 'AppAccessRoadController as ctrl'
            }
        }
    })
    .state('app.protectedarea', {
        url: '/protectedarea',
        params: { entityName: 'protectedarea' },
        ncyBreadcrumb: { label: 'Áreas Protegidas' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/mapa/mapa.html',
                controller: 'MapController as ctrl'
            }
        }
    })
    .state('tree', {
        parent: 'main',
        url: '/tree',
        //params: { entityName: 'protectedarea' },
        ncyBreadcrumb: { label: 'Tree' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/tree/tree.html',
                controller: 'treeFaunaController as ctrl'
            }
        }
    })
    .state('loading', {
        parent: 'main',
        url: '/loading',
        //params: { entityName: 'protectedarea' },
        ncyBreadcrumb: { label: 'Tree' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/loading/loading.html',
             
            }
        }
    });

    //Enable formatting of urls in html5 mode so that # does not apear in the urls
    $locationProvider.html5Mode(true);

    //Localization of the dropdown directive (nya-bs-select)
    nyaBsConfigProvider.setLocalizedText('pt-br', {
        defaultNoneSelection: 'Selecione...',
        noSearchResult: 'Pesquisa sem resultados',
        numberItemSelected: '%d itens selecionado(s)'
    });

    nyaBsConfigProvider.useLocale('pt-br');


}]);

angular.module('SGA').run(['amMoment', function (amMoment) {
    amMoment.changeLocale('pt-br');

}]);

angular.module('SGA').factory('authInterceptorService', ['$q', 'loadingService', '$location', 'currentUser', function ($q, loadingService, $location, currentUser) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var profile = currentUser.getProfile();

        if (profile.isLoggedIn) {
            config.headers.Authorization = 'Bearer ' + profile.token;
            //console.log("Sending bearer token");
        }
       
        loadingService.load++;
        

        return config;
    }

    var _responseError = function (rejection) {
        //if (rejection.status === 401) {
        //    $location.path('/login');
        //}
        loadingService.load--;
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    //  authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);

angular.module('SGA').factory('interceptorHttp', ['$q', 'messageService', 'loadingService', function ($q, messageService, loadingService) {

    return {
        response: function (response) {
           
                loadingService.load--;
            
            return response;
        },
        responseError: function (rejection) {

            //TODO :O directive monitorar a  mensagem
            //TODO: Vamos setar o Erro e o Sucesso
            loadingService.load--;
            var message = null;
            var title = 'Error';


            if ((rejection.status === -1) && (rejection.config.url !== 'http://localhost:10000/api/menu')) {
                message = 'Favor fazer o login';
                messageService.send(message, title);
            }
            if (typeof rejection.data.message === 'undefined') {
                if ((rejection.status >= 400) && (rejection.status < 500)) {

                    if (rejection.status === 400) { message = 'Usuário ou Senha estão errados'; };
                    if (rejection.status === 401) { message = 'Você não tem Autorização'; };
                    if (rejection.status === 404) { message = 'Não encontrado'; };
                    if (rejection.status === 405) { message = 'Método não Permitido'; }

                }
                else if ((rejection.status >= 500) && (rejection.status < 600)) {
                    if (rejection.status === 500) { message = 'Ocorreu um erro interno no servidor'; }
                    if (rejection.status === 503) { message = 'Serviço indisponível'; }
                    if (rejection.status === 504) { message = 'O Servidor está em Manutenção'; }

                }
            }
            else if (typeof rejection.data.message.length !== null) {
                message = rejection.data.message;
            }

            messageService.send(message, title);
            return $q.reject(rejection);
        }
    }
}]);

angular.module('SGA').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
}]);

angular.module('SGA').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('interceptorHttp');
}]);

angular.module('SGA').run(['$rootScope', '$state', 'userService', 'dialogService', 'currentUser', function ($rootScope, $state, userService, dialogService, currentUser) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        // if already authenticated...
        var isAuthenticated = currentUser.getProfile().isLoggedIn;
        // Armazena o state anterior para que quando for chamando o Botao de voltar(dialogConfirm.js) o mesmo retorne ao anterior
        dialogService.previous = fromState.url;
        // any public action is allowed
        var isPublicAction = angular.isObject(toState.data) && toState.data.isPublic === true;
        if (isPublicAction || isAuthenticated) { return; }
        // stop state change

        event.preventDefault();
        // async load user 
        userService
        .getAuthObject()
        .then(function (user) {
            var isAuthenticated = user.isAuthenticated === true;
            if (isAuthenticated) {
                // let's continue, use is allowed
                $state.go(toState, toParams)
                return;
            }
            $state.go('login');
        })
    })

}])