angular.module('SGA', ['ngResource'
                         , 'nya.bootstrap.select'
                         , 'nemLogging'
                         , 'ui-leaflet'
                         , 'ngTagsInput'
                         , 'ngFileUpload'
                         , 'ngAnimate'
                         , 'ui.bootstrap'
                         , 'ui.router'
                         , 'angularMoment'
                         , 'ngCookies'
                         , 'ncy-angular-breadcrumb'
                         , 'ngMaterial'
                         , 'ngAnimate'
                         , 'ngAria'
                         , 'cgPrompt'
                         , 'bootstrapLightbox'
                         , 'angularBootstrapNavTree'
                         
]);

angular.module('SGA').config(['$breadcrumbProvider', function ($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: '<ol class="breadcrumb">' +
            '<li><i class="fa fa-folder-open-o"></i> Sistema de Gestão Ambiental e Territorial</li>' +
           '<li ng-repeat="step in steps" ng-switch="$last || !!step.abstract" ng-class="{active: $last}">' +
            '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a>' +
            '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>' +
            '<span class="divider" ng-hide="$last"></span>' +
            '</li>' +
            '</ol>'
    });
}]);

angular.module('SGA').config(['$urlRouterProvider', '$locationProvider', '$stateProvider', 'nyaBsConfigProvider', function ($urlRouterProvider, $locationProvider, $stateProvider, nyaBsConfigProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state("main", {
            abstract: true,
            views: {
                "menu": {
                    templateUrl: 'assets/templates/menu/menu.html',
                    controller: 'MenuController as ctrl',
                },
                "header": {
                    templateUrl: 'assets/templates/menu/topHeader.html',
                    controller: 'LoginController as ctrl',
                },
                "breadcrumbs": {
                    templateUrl: 'assets/templates/menu/breadcrumbs.html',
                }
            }
        })
        .state("home", {
            parent: 'main',
            url: '/home',
            ncyBreadcrumb: { label: 'Home' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/Home.html',
                    controller: 'LoginController as ctrl',
                }
            }
        })
       .state("login", {
           parent: 'main',
           url: '/login',
           ncyBreadcrumb: { label: 'Login' },
           data: { isPublic: true },
           views: {
               "content@": {
                   templateUrl: 'assets/templates/Login.html',
                   controller: 'LoginController as ctrl',
               }
           }
       })
        .state("account", {
            parent: 'main',
            url: '/register',
            ncyBreadcrumb: { label: 'Lista de Usuario' },

            views: {
                "content@": {
                    templateUrl: 'assets/templates/account/search.html',
                    controller: 'AccountSearchController as ctrl',
                }
            }
        })
        .state('account.search', {
            url: '/search',
            ncyBreadcrumb: { label: 'Lista de Usuario' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/account/search.html',
                    controller: 'AccountSearchController as ctrl',
                }
            }
        })
            .state('account.add', {
                url: '/add',
                ncyBreadcrumb: { label: 'Cadastro de Usuario' },
                views: {
                    "content@": {
                        templateUrl: 'assets/templates/account/add.html',
                        controller: 'AccountController as ctrl',
                    }
                }
            })
         .state("account.id", {
             url: '/:email',
             ncyBreadcrumb: { label: '{{ctrl.user.person.name}}' },
             views: {
                 "content@": {
                     templateUrl: 'assets/templates/account/edit.html',
                     controller: 'AccountController as ctrl',
                 }
             }

         })
    .state("incidents",
        {
            parent: 'main',
            url: '/incidents',
            ncyBreadcrumb: { label: 'Incidentes' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/search.html',
                    data: { module: 'incidents' },
                    controller: 'IncidentSearchController as ctrl'
                }
            }
        })
    .state("incidents.search",
        {
            url: '/search',
            ncyBreadcrumb: { label: 'Pesquisar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/search.html',
                    controller: 'IncidentSearchController as ctrl'
                }
            }
        })
    .state("incidents.id",
        {
            url: '/edit/:id',
            ncyBreadcrumb: { label: '{{ctrl.code}}' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/edit.html',
                    controller: 'IncidentController as ctrl'
                }
            }
        })
    .state("incidents.add",
        {
            url: '/add',
            ncyBreadcrumb: { label: 'Adicionar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/incident/edit.html',
                    controller: 'IncidentController as ctrl'
                }
            }
        })
        .state("highwayaccessroads",
        {
            parent: 'main',
            params: { module: "ACR" },
            url: '/highwayaccessroads',
            ncyBreadcrumb: { label: 'Acesso' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/search.html',
                    data: { module: 'occupation' },
                    controller: 'HighWayAccessRoadSearchController as ctrl'
                }
            }
        })
    .state("highwayaccessroads.search",
        {
            url: '/search',
            ncyBreadcrumb: { label: 'Pesquisar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/search.html',
                    controller: 'HighWayAccessRoadSearchController as ctrl'
                }
            }
        }
    )
    .state("highwayaccessroads.id",
        {
            url: '/edit/:id',
            ncyBreadcrumb: { label: '{{ctrl.code}}' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/edit.html',
                    controller: 'HighWayAccessRoadController as ctrl'
                }
            }
        })
    .state("highwayaccessroads.add",
        {
            url: '/add',
            ncyBreadcrumb: { label: 'Adicionar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/accessroad/edit.html',
                    controller: 'HighWayAccessRoadController as ctrl'
                }
            }

        })
     .state("highwayaccessroads.report",
        {
            url: '/reports',
            ncyBreadcrumb: { label: 'Relatórios' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/report/report.html',
                    controller: 'ReportController as ctrl'
                }
            }
        })
      .state("roadsideoccupations",
        {
            parent: 'main',
            url: '/roadsideoccupations',
            params: { module: "OCR" },
            ncyBreadcrumb: { label: 'Ocupação' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/search.html',
                    controller: 'RoadsideOccupationSearchController as ctrl'
                }
            }
        })
    .state("roadsideoccupations.search",
        {
            url: '/search',
            ncyBreadcrumb: { label: 'Pesquisar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/search.html',
                    controller: 'RoadsideOccupationSearchController as ctrl'
                }
            }
        })
    .state("roadsideoccupations.id",
        {
            url: '/edit/:id',
            ncyBreadcrumb: { label: '{{ctrl.code}}' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/edit.html',
                    controller: 'RoadsideOccupationController as ctrl'
                }
            },
        })
    .state("roadsideoccupations.add",
        {
            url: '/add',
            ncyBreadcrumb: { label: 'Adicionar' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/highwayoccupation/occupation/edit.html',
                    controller: 'RoadsideOccupationController as ctrl'
                }
            }
        })

         .state("roadsideoccupations.report",
        {
            url: '/reports',
            ncyBreadcrumb: { label: 'Relatórios' },
            views: {
                "content@": {

                    templateUrl: 'assets/templates/report/report.html',
                    controller: 'ReportController as ctrl'
                }
            }
        })

        .state('roadsideoccupations.map', {
            url: '/map',
            params: { entityName: 'roadsideoccupation' },
            ncyBreadcrumb: { label: 'Mapa' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/mapa/mapa.html',
                    controller: 'MapController as ctrl'
                }
            }
        })

    .state('highways', {

        parent: 'main',
        url: '/highways',
        ncyBreadcrumb: { label: 'Rodovia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/search.html',
                controller: 'HighwaySearchController as ctrl'
            }
        }
    })
    .state('highways.search', {
        url: '/search',
        ncyBreadcrumb: { label: 'Pesquisar' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/search.html',
                controller: 'HighwaySearchController as ctrl'
            }
        }
    })
    .state('highways.id', {
        url: '/edit/:id',
        ncyBreadcrumb: { label: '{{ctrl.highway.name}}' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/edit.html',
                controller: 'HighwayController as ctrl'
            }
        }
    })
    .state('highways.add', {
        url: '/add',
        ncyBreadcrumb: { label: 'Cadastro de Rodovia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highway/add.html',
                controller: 'HighwayController as ctrl'
            }
        }
    })
    .state('report', {
        parent: 'main',
        url: '/report',
        ncyBreadcrumb: { label: 'Relatórios' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/report/relatorio.html',
                controller: 'ReportController as ctrl'
            }
        }
    })
    .state('report.relatorio', {
        url: '/relatorio',
        ncyBreadcrumb: { label: 'Pesquisar' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/report/relatorio.html',
                controller: 'ReportController as ctrl'
            }
        }
    })

        .state('forestry',{
            parent: 'main',
            url: '/forestry',
            ncyBreadcrumb: { label: 'Eng. Florestal' },
            views: {
                "content@": {
                    templateUrl: 'assets/templates/forestry/tree.html',
                    controller: 'ForestryController as ctrl'
                }
            }
        })
     
         .state('forestry.parcel', {
         })

        .state('forestry.parcel.id', {
            url: '/:id',
            ncyBreadcrumb: { label: 'Parcela {{ ctrl.Parcel.Name }}' },
            views: {
                "details@forestry": {
                    templateUrl: 'assets/templates/forestry/parcel.html',
                    controller: 'ForestryParcelController as ctrl'
                }
            }
        })


        .state('app', {
        parent: 'main',
        url: '/app',
        ncyBreadcrumb: { label: 'App Rodovia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highwayapp/highwayapp.html',
                controller: 'HighwayAppController as ctrl'
            }
        }
    })
    .state('app.accessroad', {
        url: '/accessroad',
        ncyBreadcrumb: { label: 'App de Acessos a Rovodia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highwayapp/highwayapp.html',
                controller: 'AppAccessRoadController as ctrl'
            }
        }
    })
    .state('app.accessroad.accessroadid', {
        url: '/:id',
        ncyBreadcrumb: { label: 'App de Acessos a Rovodia' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/highwayapp/appinformation.html',
                controller: 'AppAccessRoadController as ctrl'
            }
        }
    })
    .state('app.protectedarea', {
        url: '/protectedarea',
        params: { entityName: 'protectedarea' },
        ncyBreadcrumb: { label: 'Áreas Protegidas' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/mapa/mapa.html',
                controller: 'MapController as ctrl'
            }
        }
    })
    .state('tree', {
        parent: 'main',
        url: '/tree',
        //params: { entityName: 'protectedarea' },
        ncyBreadcrumb: { label: 'Tree' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/tree/tree.html',
                controller: 'treeFaunaController as ctrl'
            }
        }
    })
    .state('loading', {
        parent: 'main',
        url: '/loading',
        //params: { entityName: 'protectedarea' },
        ncyBreadcrumb: { label: 'Tree' },
        views: {
            "content@": {
                templateUrl: 'assets/templates/loading/loading.html',
             
            }
        }
    });

    //Enable formatting of urls in html5 mode so that # does not apear in the urls
    $locationProvider.html5Mode(true);

    //Localization of the dropdown directive (nya-bs-select)
    nyaBsConfigProvider.setLocalizedText('pt-br', {
        defaultNoneSelection: 'Selecione...',
        noSearchResult: 'Pesquisa sem resultados',
        numberItemSelected: '%d itens selecionado(s)'
    });

    nyaBsConfigProvider.useLocale('pt-br');


}]);

angular.module('SGA').run(['amMoment', function (amMoment) {
    amMoment.changeLocale('pt-br');

}]);

angular.module('SGA').factory('authInterceptorService', ['$q', 'loadingService', '$location', 'currentUser', function ($q, loadingService, $location, currentUser) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var profile = currentUser.getProfile();

        if (profile.isLoggedIn) {
            config.headers.Authorization = 'Bearer ' + profile.token;
            //console.log("Sending bearer token");
        }
       
        loadingService.load++;
        

        return config;
    }

    var _responseError = function (rejection) {
        //if (rejection.status === 401) {
        //    $location.path('/login');
        //}
        loadingService.load--;
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    //  authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);

angular.module('SGA').factory('interceptorHttp', ['$q', 'messageService', 'loadingService', function ($q, messageService, loadingService) {

    return {
        response: function (response) {
           
                loadingService.load--;
            
            return response;
        },
        responseError: function (rejection) {

            //TODO :O directive monitorar a  mensagem
            //TODO: Vamos setar o Erro e o Sucesso
            loadingService.load--;
            var message = null;
            var title = 'Error';


            if ((rejection.status === -1) && (rejection.config.url !== 'http://localhost:10000/api/menu')) {
                message = 'Favor fazer o login';
                messageService.send(message, title);
            }
            if (typeof rejection.data.message === 'undefined') {
                if ((rejection.status >= 400) && (rejection.status < 500)) {

                    if (rejection.status === 400) { message = 'Usuário ou Senha estão errados'; };
                    if (rejection.status === 401) { message = 'Você não tem Autorização'; };
                    if (rejection.status === 404) { message = 'Não encontrado'; };
                    if (rejection.status === 405) { message = 'Método não Permitido'; }

                }
                else if ((rejection.status >= 500) && (rejection.status < 600)) {
                    if (rejection.status === 500) { message = 'Ocorreu um erro interno no servidor'; }
                    if (rejection.status === 503) { message = 'Serviço indisponível'; }
                    if (rejection.status === 504) { message = 'O Servidor está em Manutenção'; }

                }
            }
            else if (typeof rejection.data.message.length !== null) {
                message = rejection.data.message;
            }

            messageService.send(message, title);
            return $q.reject(rejection);
        }
    }
}]);

angular.module('SGA').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
}]);

angular.module('SGA').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('interceptorHttp');
}]);

angular.module('SGA').run(['$rootScope', '$state', 'userService', 'dialogService', 'currentUser', function ($rootScope, $state, userService, dialogService, currentUser) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        // if already authenticated...
        var isAuthenticated = currentUser.getProfile().isLoggedIn;
        // Armazena o state anterior para que quando for chamando o Botao de voltar(dialogConfirm.js) o mesmo retorne ao anterior
        dialogService.previous = fromState.url;
        // any public action is allowed
        var isPublicAction = angular.isObject(toState.data) && toState.data.isPublic === true;
        if (isPublicAction || isAuthenticated) { return; }
        // stop state change

        event.preventDefault();
        // async load user 
        userService
        .getAuthObject()
        .then(function (user) {
            var isAuthenticated = user.isAuthenticated === true;
            if (isAuthenticated) {
                // let's continue, use is allowed
                $state.go(toState, toParams)
                return;
            }
            $state.go('login');
        })
    })

}])
//proj4.defs("EPSG:4326", "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");// Já definido pelo proj4
proj4.defs("EPSG:4674", "+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs");
proj4.defs("EPSG:4618", "+proj=longlat +ellps=aust_SA +no_defs");

proj4.defs("EPSG:31978", "+proj=utm +zone=18 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31979", "+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31980", "+proj=utm +zone=20 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31981", "+proj=utm +zone=21 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31982", "+proj=utm +zone=22 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31983", "+proj=utm +zone=23 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31984", "+proj=utm +zone=24 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31985", "+proj=utm +zone=25 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

proj4.defs("EPSG:32718", "+proj=utm +zone=18 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32719", "+proj=utm +zone=19 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32720", "+proj=utm +zone=20 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32721", "+proj=utm +zone=21 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32722", "+proj=utm +zone=22 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32723", "+proj=utm +zone=23 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32724", "+proj=utm +zone=24 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32725", "+proj=utm +zone=25 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");

proj4.defs("EPSG:29188", "+proj=utm +zone=18 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29189", "+proj=utm +zone=19 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29190", "+proj=utm +zone=20 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29191", "+proj=utm +zone=21 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29192", "+proj=utm +zone=22 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29193", "+proj=utm +zone=23 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29194", "+proj=utm +zone=24 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29195", "+proj=utm +zone=25 +south +ellps=aust_SA +units=m +no_defs");
angular.module('SGA').factory('accountService', ['$resource', 'settings', accountService]);

function accountService($resource, settings) {

    return {
        check: $resource(settings.sgaApiBaseUrl + 'account/validation/:email', null, {
            'query': { method: 'GET', params: { email: '@email' }, isArray: false }
        }),
        params: $resource(settings.sgaApiBaseUrl + 'accounts/searchparams', null, {
            'query': { method: 'GET', isArray: true }
        }),
        person: $resource(settings.sgaApiBaseUrl + 'account/listuser', null, {
            'query': { method: 'GET', isArray: true }
        }),
        organizationList: $resource(settings.sgaApiBaseUrl + 'account/organizationlist', null, {
            'query': { method: 'GET', isArray: true }
        }),
        singlePerson: $resource(settings.sgaApiBaseUrl + 'account/person/:id', null, {
            'put': { method: 'PUT', params: { id: '@id' } }
        }),
        listRole: $resource(settings.sgaApiBaseUrl + 'account/rolelist', null, {
            'query': { method: 'GET', isArray: true }
        }),
        contract: $resource(settings.sgaApiBaseUrl + 'account/listprefix', null, {
            'query': {method: 'GET', isArray: true}
        }),
        accountUser: $resource(settings.sgaApiBaseUrl + 'account/:email', null, {
            'query': { method: 'GET', params:{email:'@email'}, isArray: false }
        }),
        getAccountModel: $resource(settings.sgaApiBaseUrl + 'account/get', null, {
            'query': { method: 'GET' }
        }),
        saveUser: $resource(settings.sgaApiBaseUrl+ 'account/add', null, {
            'save':{method:'POST'}
        }),
        deleteAccount: $resource(settings.sgaApiBaseUrl + 'account/:id', null, {
            'delete': { method: 'DELETE', params: { id: '@id' }, isArray: false }
        })

    }
}
angular.module('SGA').factory('appService', ['$resource', 'settings', appService]);
function appService($resource, settings) {
    return {
        get: $resource(settings.sgaApiBaseUrl + 'app/accessroad', null, {
            'query': { method: 'GET', isArray: true },
            'getAccessRoadID': { method: 'GET', params: { id: '@id' }, isArray: true, url: settings.sgaApiBaseUrl + 'app/accessroad/:id' },
            'getdirect': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'app/direction' },
        }),
    }
}

angular
    .module('SGA')
    .factory('currentUser', ['$cookies' , currentUser]);

function currentUser($cookies) {
    var profile = {
        isLoggedIn: false,
        username: '',
        token: ''
    };

    var setProfile = function (username, token) {
        
        profile.username = username;
        profile.token = token;
        profile.isLoggedIn = true;
        $cookies.putObject('profile', profile);
    };

    var clearProfile = function () {

        profile.username = "";
        profile.token = "";
        profile.isLoggedIn = false;
        $cookies.remove("profile");
    };


    var getProfile = function () {
        if (!profile || (profile && !profile.isLoggedIn && $cookies.getObject('profile'))) {
           profile = $cookies.getObject('profile');
        }
        return profile;
    }

    return {
        setProfile: setProfile,
        getProfile: getProfile,
        clearProfile: clearProfile
    }
}


angular.module('SGA').service('dialogService', [dialogService]);

function dialogService($rootScope) {
    //Serviço criado para armazenar a o .state
    //anteiror visitado.
    var ctrl = this;
    ctrl.previous = null;
    

}
angular
    .module('SGA')
    .service('loadingService', [loadingService]);

function loadingService() {
    var ctrl = this;
    ctrl.load = 0;
}
angular.module('SGA').factory('menuService', ['$resource', 'settings', menuService]);

function menuService($resource, settings) {
    return {

        menu:$resource(settings.sgaApiBaseUrl + 'menu', null, {
            'query': { method: 'GET'} 
        }),
    }
}

angular
    .module('SGA')
    .service('messageService', [messageService]);

function messageService() {
    var ctrl = this;
    ctrl.messages = [];
    this.send = function (message, title) {
        ctrl.messages.push({ text: message, titl: title});
    };
    this.delete = function () {
        ctrl.messages = [];
    };
}

angular.module('SGA').factory('protectedareaService', ['$resource', 'settings', protectedareaService])
function protectedareaService($resource, settings) {
    return {
        protectedArea: $resource(settings.sgaApiBaseUrl + '/protectedarea', null, {
            'post': { method: 'POST', params: { id: '@id' }, isArray: true, url: settings.sgaApiBaseUrl + '/protectedareas' },
            'search':{method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + '/protectedareas/searchparams'},
            'getId': { method: 'GET', params: { id: '@id' }, isArray: false, url: settings.sgaApiBaseUrl + '/protectedarea/:id/reduced' },
            'getLevelOfErosion': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/leveloferosion' },
            'getNaturalRegenerationState': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/naturalregenerationstate' },
            'getProtectedCondition': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/protectedcondition' },
            'getProtectedAreaType': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/protectedareatype' },
            'getReliefType': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/relieftype' },
            'getSeasonaLity': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/seasonality' },
            'getTreeDensity': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/treedensity' },
            'getUndergrowthDensity': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/undergrowthdensity' },
            'getVegetationPosition': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'protectedarea/vegetationposition' },
            
        })
    }
}
angular.module('SGA').factory('reportService', ['$resource', 'settings', reportService]);

function reportService($resource, settings) {

    return {

        //Função que retorna a lista de Contratos   
        prefix: $resource(settings.sgaApiBaseUrl + 'reports/prefixes', null, {
            'query': { method: 'GET', isArray: true } //is Array foi adicionado para ele aceitar o retorno de uma array de objeto
        }),
        //Função que retorna a lista com as Informações para preenchimento
        list: $resource(settings.sgaApiBaseUrl + 'reports?idcontract=:idcontract&module=:module', null,
            { 'query': { method: 'GET', params: { idcontract: '@idcontract',module:'@module' }, isArray: true } }
         ),
        //Envia para a API as informações para se fazer o download
        renderExcel: $resource(settings.sgaApiBaseUrl + 'reports/renderexcel', null,
            { 'save': { method: 'POST' } }
         ),
        renderPdf: $resource(settings.sgaApiBaseUrl + 'reports/renderpdf', null,
            { 'save': { method: 'POST'} }
         )
         
    }


}
/*
*
* Factory: SearchService
* Author: Jos Bol
* Function: Creates instances of a search service based on the entity name
* Uses: Angular standard services, Settings
*/

angular.module('SGA').factory('searchService', [
       '$http', '$timeout', '$location', 'settings', function ($http, $timeout, $location, settings,$state) {

           /*Constructor, accepts a string with an entityname (ex. "Incident") */
           function Instance(entityName) {

               this.url = settings.sgaApiBaseUrl + entityName + 's';
               this.entityName =  entityName;
               this.results = [];
               this.currentSearchType = null;
               this.currentArgument = "";
               this.searchTypes = [];

           }
           
           /* Gets the possible search parameters from the webservice */
           //TODO: move this into the constructor to make it private. Should onyl be called once anyway
           Instance.prototype.getParams = function () {
               var self = this;
               console.log("updating params");
               $http.get(this.url + '/searchparams'
               ).then(function (response) {
                   self.searchTypes = response.data;
                   return response;
               });
           }

           /* Processes a request to update the results list. OBS: Anti-hammering, using a configurable delay found in the settings service */
           Instance.prototype.updateList = function () {
               console.log("update list");
               var self = this;
               var par = { Type: this.currentSearchType, Argument: this.currentArgument };

               if (this.searchPending) {
                   $timeout.cancel(this.searchPending);
               }

               //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
               this.searchPending = $timeout(
                   function () {
                       console.log("getting list");

                       $http.post(self.url, par)
                           .then(function (response) {
                               self.results = response.data;
                           });
                   }, settings.autoCompleteDelay); //Configurable delay used here


           };

           //Set the current searchType
           Instance.prototype.setSearchType = function (type) {
               var self = this;
               //If the old and new searchtypes are incompatible, empty the search expression
               if (self.currentSearchType && self.currentSearchType.dataType !== type.dataType) {
                   self.currentArgument = "";
               }

               //Set the new search expression
               self.currentSearchType = type;

               //Update the list
               self.updateList();

           }

           Instance.prototype.redirectToItem = function (item) {
               $location.path($state.current.data.module + '/' + this.entityName + 's/' + item.id);
               $location.replace();
           }

           //Return the new instance, already
           return {
               Instance: function (entityName) {
                   var i = new Instance(entityName);
                   i.getParams();
                   return i;
               }
           };

       }
]);
//Produção

//angular.module('SGA').value('settings',
//    {
//       // BaseURL of the SGA Web API URL
//        sgaApiBaseUrl: "http://api.sga.essati.com.br/api/",
//        sgaTokenUrl: "http://api.sga.essati.com.br/Token",
//        baseUrl: "http://sga.essati.com.br/api/",
//        //Requests for autocomplete are only send after the user input has stopped for X miliseconds.
//        autoCompleteDelay: 500
//    });
    

//Desenvolvimento

angular.module('SGA').value('settings',
    {
        //BaseURL of the SGA Web API URL
        sgaApiBaseUrl: "http://localhost:10000/api/",
        sgaTokenUrl: "http://localhost:10000/Token",
        baseUrl: "http://localhost:64479/api/",
        //Requests for autocomplete are only send after the user input has stopped for X miliseconds.
        autoCompleteDelay: 500
    });


angular
    .module('SGA')
    .factory('userAccount',
    [
        '$resource',
        'settings',
        userAccount
    ]);

function userAccount($resource, settings) {
    return {
        registration: $resource(settings.baseUrl + 'Account/Register', null,
        {
            'registerUser': { method: 'POST' }
        }),
        login: $resource(settings.sgaTokenUrl, null,
        {
            'loginUser': {
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                transformRequest: function (data, headersGetter) {
                    var str = [];
                    for (var d in data)
                        str.push(encodeURIComponent(d) + '=' +
                            encodeURIComponent(data[d]));
                    return str.join('&');
                }

            }
        }),
        test: $resource(settings.sgaApiBaseUrl + 'Account/TestRestricted', null, {
            'test': { method: 'POST' }
        }),

        userInfo: $resource(settings.baseUrl + 'Account/UserInfo', null, {
            'userInfo': { method: 'GET' }
        })
    }
}


angular.module('SGA').factory('userService', ['$q', '$timeout', function ($q, $timeout) {
    var user = undefined;
    return {
        getAuthObject: function () {
            var deferred = $q.defer();
            if (user) { return $q.when(user); }
            $timeout(function () {
                user = { isAuthenticated: false };
                deferred.resolve(user)
            }, 500)
            return deferred.promise;
        },
        isAuthenticated: function () {
            return user !== undefined && user.isAuthenticated;
        }
    };
    console.log(user.isAuthenticated);
}]);
angular.module('SGA').factory('mapService', ['$resource', 'settings', mapService])
function mapService($resource, settings) {
    return {
        mapArea: $resource(settings.sgaApiBaseUrl + '/:entityName', null, { 
            'get': { method: 'GET', params: { id: '@id', entityName: '@entityName' }, isArray: false, url: settings.sgaApiBaseUrl + '/:entityName/:id/reduced' },
        })
    }
}
angular.module('SGA').directive('direction', function () {
    return {
        restric: 'E',
        scope: {},
        bindToController: {

        },
        templateUrl: "",
        controllerAs: '',
        controller: ['', function () {

        }]
    }

})

angular.module('SGA').directive('reportboolean', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            parameter: '=',
        },
        templateUrl: "assets/templates/report/reportboolean.html",
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this
            

           
        }
    };
});
angular.module('SGA').directive('reportdate', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {   //as variaveis que serão usadas no html para receber o dado 
            parameter: '='
        },
        templateUrl: "assets/templates/report/reportdate.html",
        controllerAs: 'ctrl',
        controller: function () {   //Na function eu adiciono se necessario os modulos($http, settings, $stateParameters) que vai precisar
            var ctrl = this
            this.today = function () {
                this.date = new Date();
                this.datafim = new Date();
                
            };
            this.showWeeks = false;
            this.today();
            this.open = function ($filter) {
                ctrl.popup.opened = true;
            };
            this.popup = {
                opened: false,
                
            };
        }
    }

});

angular.module('SGA').directive('reportinteger', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            parameter: '=',
            
        },
        templateUrl: "assets/templates/report/reportinteger.html",
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this;


        }
    };
});
angular.module('SGA').directive('reportstring', function () {
    return {
        restric: 'E',
        scope: {},
        bindToController:{
            parameter:'=',
        },
        templateUrl: "assets/templates/report/reportstring.html",
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this
            


        }
    };
});
angular.module("SGA").directive("contenteditable", function () {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }

            ngModel.$render = function () {
                element.html(ngModel.$viewValue || "");
            };

            element.bind("blur keyup change", function () {
                scope.$apply(read);
            });
        }
    };
});
angular.module('SGA').directive('coordinates', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            sgaCoordinates: '=',
            sgaMarkers: '=',
            sgaAdd: '&',
            sgaDelete: '&',
            sgaChange: '&',
            sgaInputSrid: "="
        },
        replace: true,
        templateUrl: "assets/js/directives/templates/coordinates.html",
        controllerAs: 'ctrl',
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            ctrl.currentDatum = 0;
            ctrl.currentType = 0;
            ctrl.test = 0;
            ctrl.handleSridChange = function (newSrid, oldSrid) {
                var sridIndexes = ctrl.lookupSRIDToIndex(newSrid);
                var sridIndexesOld = ctrl.lookupSRIDToIndex(oldSrid);
                if (sridIndexes.found) {
                    ctrl.currentDatum = sridIndexes.datum;
                    ctrl.currentType = sridIndexes.type;
                } else {
                    console.log("Não foi possível determinar os indices do SRID!");
                    //Setando para geográfico/SIRGAS
                    ctrl.currentDatum = 0;
                    ctrl.currentType = 0;
                }

                //No caso de alteração de coordenadas projetadas para geográficos ou vice versa, altera o "Object Type Descriptor" de cada coordenada
                //O API precisa disso para a deserialização
                var objType = null;
                if (sridIndexesOld.type === 0 && sridIndexes.type !== 0) {
                    console.log("Alterando OTD das coordenadas para XY");
                    // coord.$type = "Essati.SGA.Models.GeneralComplexTypes.PointXY, Essati.SGA.Models";
                    objType = "Essati.SGA.Models.GeneralComplexTypes.PointXY, Essati.SGA.Models";
                } else if (sridIndexesOld.type !== 0 && sridIndexes.type === 0) {
                    console.log("Alterando OTD das coordenadas para LatLong");
                    objType = "Essati.SGA.Models.GeneralComplexTypes.PointLatLng, Essati.SGA.Models";
                }
                

                if (!!oldSrid) {
                    console.log("Reprojetando as coordenadas");
                    for (var i = 0; i < ctrl.sgaCoordinates.length; i++) {
                        var coord = ctrl.sgaCoordinates[i];

                        //Seta o tipo (pode ter mudado ou não
                        coord.$type = objType || coord.$type;

                        //Reprojeta a coordenada
                        var projected = proj4("EPSG:" + oldSrid, "EPSG:" + newSrid, [coord.lng, coord.lat]);

                        //Aredonda para no máximo 6 decimais, para limpar artefatos de calculo de floating point. 6 decimais permite um precição de aprox 10cm.
                        //o + serve para tirar os zeros finais
                        coord.lng = +(Math.round(projected[0] + "e+6") + "e-6");
                        coord.lat = +(Math.round(projected[1] + "e+6") + "e-6");
                    }
                }
            };

            ctrl.updateCoordinate = function (coord, index) {
                var marker = this.sgaMarkers[index];
                if (!isNaN(coord.lat) && coord.lat !== null && !isNaN(coord.lng) && coord.lng !== null) {
                   
                    //Verificar se o SRID atual é 4326, se não for, projeta primeiro
                    //Atenção: Apesar do fato que se fala de Lat/Long, a ordem "correta" (discussão eterna), é Long/Lat. É assim que o proj4 espera tb
                    if (ctrl.sgaInputSrid === 4326) {
                        marker.lng = coord.lng;
                        marker.lat = coord.lat;
                    } else {
                        var projected = proj4("EPSG:" + ctrl.sgaInputSrid, "WGS84", [coord.lng, coord.lat]);
                        marker.lng = projected[0];
                        marker.lat = projected[1];
                        
                    }
                 

                  

                    this.sgaChange();
                }
            }

            ctrl.deleteCoordinate = function (index) {

                var marker = this.sgaMarkers.splice(index, 1);
                var coord = this.sgaCoordinates.splice(index, 1);

                this.sgaDelete(coord, marker);
                this.sgaChange();
            };

            ctrl.addCoordinate = function () {
                this.sgaMarkers.push({});
                this.sgaCoordinates.push({ $type: "Essati.SGA.Models.GeneralComplexTypes.PointLatLng, Essati.SGA.Models" }); //Necessario para a deserialização no lado do servidor
                this.sgaAdd();
                this.sgaChange();

            };

            ctrl.lookupIndexToSRID = function (datumIndex, typeIndex) {
                return ctrl.sridLookup[datumIndex][typeIndex];
            }

            ctrl.lookupSRIDToIndex = function (srid) {
                var datumIndex = 0;
                var typeIndex = 0;
                var currentDatum = []; // Lista de tipos 
                for (datumIndex = 0; datumIndex < ctrl.sridLookup.length; datumIndex++) {
                    currentDatum = ctrl.sridLookup[datumIndex];
                    for (typeIndex = 0; typeIndex < currentDatum.length; typeIndex++) {
                        if (currentDatum[typeIndex] === srid) {
                            return { found: true, datum: datumIndex, type: typeIndex };
                        }
                    }

                }
                return { found: false };
            }

            


            //Tabela de determinação do codidos do EPSG para o SRID a usar
            ctrl.sridLookup = [
                [
                    4674, //	GCS SIRGAS 2000 (geográfico)
                    31978, //	SIRGAS 2000 / UTM zone 18S
                    31979, //	SIRGAS 2000 / UTM zone 19S
                    31980, //	SIRGAS 2000 / UTM zone 20S
                    31981, //	SIRGAS 2000 / UTM zone 21S
                    31982, //	SIRGAS 2000 / UTM zone 22S
                    31983, //	SIRGAS 2000 / UTM zone 23S
                    31984, //	SIRGAS 2000 / UTM zone 24S
                    31985 //	SIRGAS 2000 / UTM zone 25S
                ],
                [
                    4326, //	GCS WGS84 (geográfico)
                    32718, //	WGS 84 / UTM zone 18S
                    32719, //	WGS 84 / UTM zone 19S
                    32720, //	WGS 84 / UTM zone 20S
                    32721, //	WGS 84 / UTM zone 21S
                    32722, //	WGS 84 / UTM zone 22S
                    32723, //	WGS 84 / UTM zone 23S
                    32724, //	WGS 84 / UTM zone 24S
                    32725 //	WGS 84 / UTM zone 25S
                ],
                [
//SAD69 não pode ser mais usado para levantamentos novos, mas incluimos para ter compatibilidade com dados levantados no passado
                    4618, //	GCS SAD69 (geográfico)
                    29188, //	SAD69 / UTM zone 18S
                    29189, //	SAD69 / UTM zone 19S
                    29190, //	SAD69 / UTM zone 20S
                    29191, //	SAD69 / UTM zone 21S
                    29192, //	SAD69 / UTM zone 22S
                    29193, //	SAD69 / UTM zone 23S
                    29194, //	SAD69 / UTM zone 24S
                    29195 //	SAD69 / UTM zone 25S
                ]
            ];

            ctrl.coordinateTypes = [{ id: 0, name: "Geográfico" },
                                    { id: 1, name: "UTM 18S" },
                                    { id: 2, name: "UTM 19S" },
                                    { id: 3, name: "UTM 20S" },
                                    { id: 4, name: "UTM 21S" },
                                    { id: 5, name: "UTM 22S" },
                                    { id: 6, name: "UTM 23S" },
                                    { id: 7, name: "UTM 24S" },
                                    { id: 8, name: "UTM 25S" },
                                   ];
        

            //var srid = 0;
            //srid = ctrl.lookupSRIDToIndex(ctrl.sgaInputSrid);
            //if (srid.found) {
            //    ctrl.currentDatum = srid.datum;
            //    ctrl.currentType = srid.type;
            //} else {
            //    console.log("Não foi possível determinar o SRID!");

            //    //Setando para geográfico/SIRGAS
            //    ctrl.currentDatum = 0;
            //    ctrl.currentType = 0;
            //}


            $scope.$watch(
                  function watchSrid(scope) {
                      // Return the "result" of the watch expression.
                      return (ctrl.sgaInputSrid);
                  },
                  function handleChange(newValue, oldValue) {
                      ctrl.handleSridChange(newValue, oldValue);
                  }
              );
            
            $scope.$watch(
                function watchSrid(scope) {
                    // Return the "result" of the watch expression.
                    return (ctrl.currentDatum);
                },
                function handleChange(newValue, oldValue) {
                    ctrl.sgaInputSrid = ctrl.lookupIndexToSRID(ctrl.currentDatum, ctrl.currentType);
                }
            );

            $scope.$watch(
               function watchSrid(scope) {
                   // Return the "result" of the watch expression.
                   return (ctrl.currentType);
               },
               function handleChange(newValue, oldValue) {
                   ctrl.sgaInputSrid = ctrl.lookupIndexToSRID(ctrl.currentDatum, ctrl.currentType);
               }
           );

        }
    ]};
});


angular.module('SGA').directive('dialogConfirm', ['prompt', '$rootScope', '$location', '$state', 'dialogService' ,function (prompt, $rootScope, $location, $state, dialogService) {

    //Modelo da chamada do directive
    //<dialog-confirm title-temp="Salvar" text-temp="Você deseja salvar ?" delete-temp="true"></dialog-confirm>

    return {
        restrict: 'E',
        bindToController: {
            titleTemp: '@titleTemp',
            textTemp: '@textTemp',
            deleteTemp: '@deleteTemp',
            sgaSave: '&',
            sgaDelete: '&'

        },
        scope: {},
        templateUrl: "assets/templates/Dialog/dialogConfirm.html",
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this;
            ctrl.delete = ctrl.deleteTemp; //Check para liberar o botão de Apagar
            ctrl.backPage = backPage; //Função para o botão voltar Pagina
            ctrl.showConfirm = showConfirm; //Função para Mostrar o Dialog Confirm
            ctrl.showDelete = showDelete; //Função para Layout de Deleter Arquivo
            
            //Criado um Objeto que armazena as configurações que vai aparecer no Layout
            ctrl.options = {
                title: ctrl.titleTemp,
                message: ctrl.textTemp,
                input: false,
                buttons: [
                { label: 'Cancelar', cancel: true },
                { label: 'OK', primary: true }
                ]
            };

            function showDelete() {
                ctrl.options.title = 'Deletar',
                ctrl.options.message = 'Você deseja realmente apagar ?'
                var option = angular.copy(ctrl.options);
                prompt(option).then(
                    function () {
                    ctrl.sgaDelete(); //Chama a função delete no Controller
                }, function () {                   
                    //Quando o usuário cancela 
                })
            }
            function showConfirm() {          
                var option = angular.copy(ctrl.options);
                prompt(option).then(function () {
                    ctrl.sgaSave(); //Chama a função Salvar no Controller
                }, function () {                    
                    //Quando o usuário cancela 
                })
            }
           //Função que chama o state anterior visitado
            function backPage() {               
                $location.path(dialogService.previous); //Chama o state anterior declarado no app.js
            }
        }
    }
}])
angular.module('SGA').directive('loading', ['loadingService', function (loadingService) {
    return {
        restrict: 'E',
        controllerAs: 'ctrl',
        scope: {},
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            //contador
            ctrl.loadStatus = null;
            $scope.$watch(
                function () {
                    return loadingService.load;
                },
                function (newVal) { //Quando o valor muda esta função é executada
                    ctrl.loadStatus = !!newVal;
                    console.log(ctrl.loadStatus);
                })
        }],
        templateUrl: "assets/js/directives/templates/loading.html",
    }
}])

angular.module('SGA').directive('sgaMap', function (searchService) {
    return {
        restric: 'E',
        scope: {},
        bindToController: {

            results: '=sgaResults',

        },
        templateUrl: "assets/js/directives/templates/mapa.html",
        controllerAs: 'ctrl',
        controller: ['$scope', 'leafletMarkerEvents', 'protectedareaService', 'leafletData',
    function ($scope, leafletMarkerEvents, protectedareaService, leafletData) {

        var ctrl = this;
        ctrl.markersMap = {};

        //
        $scope.$watchCollection("ctrl.results", function (newValue, oldValue) {
            leafletData.getDirectiveControls().then(function (controls) {
                controls.markers.create({}, ctrl.markersMap);
                var markers = ctrl.convertToMarkers(newValue);
                controls.markers.create(markers, ctrl.markersMap);
                ctrl.markersMap = markers;
            });
        });
        ctrl.convertToMarkers = function (points) {
            return points.reduce(function (total, p) {
                
                if (p.position.type === 'Point') {
                    total['a' + p.id] = {
                        id: p.id,
                        code: p.code,
                        lat: p.position.lat,
                        lng: p.position.lng,
                    };
                } else if (p.position.type === 'MultiPoint') {
                    for (var i = 0; i < p.position.points.length; i++) {
                        var pp = p.position.points[i];
                        total['a' + p.id + 'i' + i] = {
                            id: p.id,
                            code: p.code || '',
                            lat: pp.lat,
                            lng: pp.lng,
                        };

                    }

                }

                
                return total;
            }, {});

        };
        ctrl.map = {
            center: {
                lat: -22.3169854,
                lng: -43.41331029,
                zoom: 9
            }
        };
    }]
    }
});

angular.module('SGA').directive('message', ['messageService', 'prompt', function (messageService, prompt) {
    return {
        restrict: 'E',
        controllerAs: 'ctrl',
        scope: {},
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            ctrl.msg = null;
            ctrl.delete = function () {
                messageService.delete();
                ctrl.msg = null;
                ctrl.log.length = 0;
            }
            $scope.$watch(
                function () {
                    ctrl.log = messageService.messages;
                    return ctrl.log.length;
                },
                function (newVal) {
                    ctrl.msg = ctrl.log;
                    if (ctrl.msg.length > 0) {
                        prompt({
                            title: ctrl.msg[0].titl,
                            message: ctrl.msg[0].text,
                            input: false,
                            buttons: [
                                    { label: 'OK', primary: true }
                            ]
                        }).then(function () { ctrl.delete(); }), function () { ctrl.delete() }
                    }
                })
        }]
    }
}])

angular.module('SGA').directive('photoList',['Lightbox', function (Lightbox) {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            photos: '=sgaPhotos',
            change: '&sgaChange',
            Add: '=sgaAdd', //feito para removar o botão de ADD quando só quiser mostrar as fotos,
            showControls: '=sgaShowControls'
        },
        replace: true,
        templateUrl: "assets/js/directives/templates/photos.html",
        controllerAs: 'ctrl',
        controller: ['Upload','settings', function (Upload, settings) {
            var ctrl = this;
            this.fileQueue = [];
            ctrl.viewAdd = ctrl.Add; //feito para removar o botão de ADD foto quando só quiser mostrar as fotos. tem um ng-show 
            // upload on file select or drop
            this.upload = function (file) {
                Upload.upload({
                    url: settings.sgaApiBaseUrl + 'temp/photo',
                    data: { file: file }
                }).then(function (resp) {
                    //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                    ctrl.photos.push.apply(ctrl.photos, resp.data);
                    ctrl.fileQueue = [];
                    ctrl.change();
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    var progressPercentage = Math.min(parseInt(100.0 * evt.loaded / evt.total));
                    file.progress = progressPercentage;
                    // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };

            // for multiple files:
            this.uploadFiles = function (files) {
                if (files && files.length) {
                    for (var i = 0; i < files.length; i++) {
                        ctrl.upload(files[i]);
                    }
                }
            };

            this.deletePhoto = function (photoIndex) {
                var photo = ctrl.photos[photoIndex];
                ctrl.shiftPhoto(photoIndex, ctrl.photos.length - photoIndex - 1);
                photo.state = 'Deleted';
                ctrl.change();
            };

            this.shiftPhoto = function (photoIndex, numPositions) {

                //Once deleted, don't modify the state
                if (ctrl.photos[photoIndex].state !== 'Deleted') {
                    ctrl.photos[photoIndex].state = 'Modified';
                }

                if (numPositions === -1) { //normal backshifting
                    if (ctrl.photos[photoIndex + numPositions].state !== 'Deleted') {
                        ctrl.photos[photoIndex + numPositions].state = 'Modified';
                    }
                } else { //normal forward shifting, plus delete jumps
                    for (var i = 0; i <= numPositions; i++) {
                        if (ctrl.photos[photoIndex + i].state !== 'Deleted') {
                            ctrl.photos[photoIndex + i].state = 'Modified';
                        }
                    }
                }
                
                //swap the position of the photo
                ctrl.photos.splice(photoIndex + numPositions, 0, ctrl.photos.splice(photoIndex, 1)[0]);

                //Fire the change event
                ctrl.change();
            };

            this.markChanged = function (photoIndex) {
                if (ctrl.photos[photoIndex].state !== 'Deleted' && ctrl.photos[photoIndex].state !== 'Added') {
                    ctrl.photos[photoIndex].state = 'Modified';
                }
            }
            this.openLightboxModal = function (index) {
                
                var arrayPhoto = [];
                for (var i = 0; i < ctrl.photos.length; i++) {
                    arrayPhoto.push({
                        description: ctrl.photos[i].description,
                        url: ctrl.photos[i].filePath,
                        thumbnailPath: ctrl.photos[i].thumbnailPath
                    })
                }
                Lightbox.openModal(arrayPhoto, index);
           
            };

        }

    ]};
}]);
angular.module('SGA').directive('searchbar', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            sgaSearchService: '='
        },
        replace: true,
        template:
                '<div class="well">' +
                '<div class="input-group col-sm-6">' +
                '<div class="input-group-btn btn-group search-panel" uib-dropdown>' +
                '<button id="single-button" type="button" class="btn btn-primary" uib-dropdown-toggle>' +
                '{{ctrl.sgaSearchService.currentSearchType.description || "Filtrar por" }} <span class="caret"></span>' +
                '</button>' +
                '<ul class="uib-dropdown-menu" role="menu" aria-labelledby="single-button">' +
                '<li ng-repeat="t in ctrl.sgaSearchService.searchTypes" role="menuitem"><a ng-click="ctrl.setSearchType(t)">Por {{t.description}}</a></li>' +
                '</ul>' +
                '</div>' +
                '<input ng-readonly="!ctrl.sgaSearchService.currentSearchType" id="search_box" class="form-control" name="x" ng-model="ctrl.sgaSearchService.currentArgument" ng-change="ctrl.argumentChange()" placeholder="{{ctrl.sgaSearchService.currentSearchType ? \'Pesquisar...\' : \'Escolhe o tipo de pesquisa...\' }}">' +
                '</div>' +
                '</div>',
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this;

            ctrl.argumentChange = function () {
                ctrl.sgaSearchService.updateList();
                console.log("ctrl.sgaSearchService.updateList();");
            };
            ctrl.setSearchType = function (t) {
                ctrl.sgaSearchService.setSearchType(t);
                console.log(" ctrl.sgaSearchService.setSearchType(t);");
            };

        }

    };
});
angular.module('SGA').directive('sgaSelect', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            items: '=sgaItems',
            change: '&sgaChange',
            selectedValue: '=sgaSelectedValue',
            textField: '@sgaTextField',
            valueField: '@sgaValueField',
            emptyMessage: '@sgaEmptyMessage'
        },
        replace: true,
        templateUrl: "assets/js/directives/templates/select.html",
        controllerAs: 'ctrl',
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            ctrl.textField = ctrl.textField || "name";
            ctrl.valueField = ctrl.valueField || "id";
            ctrl.emptyMessage = ctrl.emptyMessage || "Itens não encontrados";
            
            $scope.$watchCollection(function () {
                return ctrl.items;
            }, function () {

                if (!ctrl.items) {
                    ctrl.selectedValue = null;
                }else if (ctrl.items.length === 1) {
                    ctrl.selectedValue = ctrl.items[0][ctrl.valueField];
                } else if (ctrl.items.length === 0) {
                    ctrl.selectedValue = null;
                };
            });

            $scope.$watch(
                   "ctrl.selectedValue",
                   function handleChange(newValue, oldValue) {
                       ctrl.change();
                   }
               );
          
        }
        
    ]};
});
angular.module('SGA').directive('smartFloat', function () {
    var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue) {
                if (FLOAT_REGEXP.test(viewValue)) {
                    ctrl.$setValidity('float', true);
                    if (typeof viewValue === "number")
                        return viewValue;
                    else
                        return parseFloat(viewValue.replace(',', '.'));
                } else {
                    ctrl.$setValidity('float', false);
                    return undefined;
                }
            });
        }
    };
});
angular.module('SGA').directive('sgaInformation', function (searchService) {
    return {
        restric: 'E',
        scope: {},
        bindToController: {

            markerInf: '=sgaMarkerinf',

        },
        templateUrl: "assets/js/directives/templates/informationMap.html",
        controllerAs: 'ctrl',
        controller: ['$scope', function ($scope) {}],
    }
})
angular.module('SGA').controller('AccountController', [
                    '$timeout'
                    , '$q'
                    , '$state'
                    , '$http'
                    , '$resource'
                    , 'searchService'
                    , 'settings'
                    , 'dialogService'
                    , 'messageService'
                    , '$scope'
                    , '$filter'
                    , '$stateParams'
                    , 'accountService'
                    , 'currentUser',
                    function (
                        $timeout,
                        $q,
                        $state,
                        $http,
                        $resource,
                        searchService,
                        settings,
                        dialogService,
                        messageService,
                        $scope,
                        $filter,
                        $stateParams,
                        accountService,
                        currentUser) {
                        var ctrl = this;
                        this.listUser = [];
                        this.nomepapel = null;
                        this.checkemail = null;
                        this.checkpass = null;

                        //listar os Person

                        //Verifica se o usuário já existe
                        ctrl.check = function () {

                            var val = $http.get(settings.sgaApiBaseUrl + 'account/validation/' + ctrl.getuser.email).success(function (data) {
                                ctrl.checkuser = data;
                            });
                        }

                        //Listar Role do Usuário Substituir userRoler para accountUser

                        ctrl.user = accountService.accountUser.query({ email: $stateParams.email || 0 });

                        //Listar os  Papeis
                        ctrl.listroles = accountService.listRole.query();

                        ctrl.listcontract = function (query) {
                            return accountService.contract.query().$promise;
                        };

                        //Listar a Organização
                        ctrl.organization = accountService.organizationList.query();
                        
                        //Pegar o template do Modelo Usuário para Salvar
                        ctrl.getuser = accountService.getAccountModel.query();
                        

                        //this.save = function (value) { return accountService.saveUser.save(value); }
                        ctrl.save = function () {
                            accountService.saveUser.save(this.getuser,
                             function () {
                                 var title = 'Salvo';
                                 var message = 'Usuário salvo com sucesso';
                                 messageService.send(message, title);
                                 $state.reload();
                             },
                            function () {
                                var title = 'Problema';
                                var message = 'Problema ao salvar, contate o administrador';
                                messageService.send(message, title);
                            });

                        }
                        //accountService.save.put();

                        //Deleta Usuario
                        ctrl.delete = function (value) {
                            accountService.deleteAccount.delete({ id: value },
                                 function () {
                                     var title = 'Apagado';
                                     var message = 'Usuário apagado com sucesso';
                                     messageService.send(message, title);
                                     $state.go('account', {}, { reload: true });
                                     
                                     
                                 },
                                function () {
                                    var title = 'Problema';
                                    var message = 'Problema ao apagar, contate o administrador';
                                    messageService.send(message, title);
                                    
                                });
                        }

                        //Enviar os dados para serem salvos

                        ctrl.addRole = function () {
                            ctrl.user.userRoles = ctrl.user.userRoles || [];
                            var i = ctrl.user.userRoles;
                            i.push({
                                idrole: "",
                                contractList: "",
                                state: 'Added',
                                isAdmin: "",
                            })

                        }
                        ctrl.role = null;

                        ctrl.changerole = function (role) {
                            role.state = 'Modified';
                        }
                        ctrl.deleterole = function (role) {
                            role.state = 'Deleted';
                        }
                        ctrl.saveAlter = function () {

                            accountService.saveUser.save(
                                this.user,
                                function () {
                                    var title = 'Salvo';
                                    var message = 'Usuário salvo com sucesso';
                                    messageService.send(message, title);
                                },
                            function () {
                                var title = 'Problema';
                                var message = 'Problema ao salvar, contate o administrador';
                                messageService.send(message, title);
                            });
                        }
                        ctrl.roledel = function () {
                            accountService.saveUser.save(ctrl.role,
                                function () {
                                    var title = 'Salvo';
                                    var message = 'Usuário salvo com sucesso';
                                    messageService.send(message, title);
                                },
                            function () {
                                var title = 'Problema';
                                var message = 'Problema ao salvar, contate o administrador';
                                messageService.send(message, title);
                            });
                        }
                        this.add = function () {
                            var i = ctrl.getuser.userRoles || [];
                            if (i.length === 0) {
                                i.push({
                                    idrole: "",
                                    contractList: "",
                                    state: 'Added',
                                    isAdmin: "",
                                });
                            } else {
                                i.push({
                                    idrole: "",
                                    contractList: "",
                                    state: 'Added',
                                    isAdmin: "",
                                });
                            }
                        }




                    }]);

angular.module('SGA').controller('AccountSearchController', [
                    '$timeout'
                    , '$q'
                    , '$state'
                    , '$http'
                    , '$resource'
                    , 'searchService'
                    , 'settings'
                    , '$scope'
                    , '$filter'
                    , '$stateParams'
                    , 'accountService'
                    , 'currentUser',
                    function (
                        $timeout,
                        $q,
                        $state,
                        $http,
                        $resource,
                        searchService,
                        settings,
                        $scope,
                        $filter,
                        $stateParams,
                        accountService,
                        currentUser) {
                        var ctrl = this;
                      

                        //listar os Person
                        this.person = accountService.person.query();
                        ctrl.search = searchService.Instance('account');
                        //console.log(ctrl.search);
                        ctrl.parametros = accountService.params.query();
                    }]);
angular.module('SGA').controller('AppAccessRoadController', [
    '$http',
    '$sce',
    'searchService',
    'settings',
    '$scope',
    '$stateParams',
    'appService',
    function ($http,
    $sce,
    searchService,
    settings,
    $scope,
    $stateParams,
    appService) {
        var ctrl = this;
        ctrl.markers = [];
        ctrl.state = "app.accessroadid";
        ctrl.accessRoad = appService.get.query(null, function () {
            var i = 0;
            while (i < ctrl.accessRoad.length) {
                if (ctrl.markers.length == ctrl.accessRoad.length) {
                    break;
                }
                //if (ctrl.accessRoad[i].position.length == 0) {
                //    console.log("Position Vazio");
                //    break
                //}
                //else if (ctrl.accessRoad[i].postion.points[0] == null) {
                //    console.log("Points null");
                //    break
                //}
                //else {
                    ctrl.markers.push({ //tratar caso seja nulo
                        id: ctrl.accessRoad[i].idaccessRoad,
                        lat: ctrl.accessRoad[i].position.points[0].lat,
                        lng: ctrl.accessRoad[i].position.points[0].lng,
                    })
                //}
                i++;
            }
        });

        ctrl.temp = [];
        ctrl.idaccessroad = appService.getAccessRoadID.query({ id: $stateParams.id || 0 });
        console.log(ctrl.idaccessroad);
        $http.get(settings.sgaApiBaseUrl + 'app/accessroad/' + $stateParams.id).then(
                        function (data) {
                            ctrl.temp.push(data.data);
                            
                            return data.data;

                        }
            );
        
        

    }]);
/*
$http - Get, Put, Post, Delete


*/
angular.module('SGA').controller('HighwayController', [
    '$http',
    'searchService',
    'settings',
    'Highway',
    'dialogService',
    'Upload',
    'messageService',
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    
    HighwayController]);


function HighwayController(
    $http,
    searchService,
    settings,
    Highway,
    dialogService,
    Upload,
    messageService,
    $rootScope,
    $scope,
    $state,
    $stateParams
    
          ) {

    var ctrl = this;

    //ctrl.add = add;
    ctrl.save = save;
    ctrl.delete = remove;
    ctrl.featureSelect = featureSelect;
    ctrl.convertSpatial = convertSpatial;
    ctrl.upload = upload;
   
    //this.search = searchService.Instance('highway');

    ctrl.highway = Highway.query({ id: $stateParams.id || 0 },
        function (h) {
            ctrl.map.paths = {};
            ctrl.map.paths[ctrl.highway.shape.type] = ctrl.convertSpatial(ctrl.highway.shape);
        }, function () {
           
        });

    // Create / Update
    function save() {
        Highway.save(
            this.highway,
           function () {
               var title = 'Salvo';
               var message = 'Arquivo salvo com sucesso';
               messageService.send(message, title);
           },
        function () {
            var title = 'Problema';
            var message = 'Problema ao salvar, contate o administrador';
            messageService.send(message, title);
        });

    }

    //Delete
    function remove() {       
        Highway.delete({ id: $stateParams.id },
        function () {
            var title =  'Apagado';
            var message = 'Arquivo apagado com sucesso';
            messageService.send(message, title);
        },
        function () {
            var title = 'Problema';
            var message = 'Problema ao apagar, contate o administrador';
            messageService.send(message, title);
        });
    }
    this.map = {
        center: {
            lat: -22.92045269,
            lng: -43.20854187,
            zoom: 5
        },
        layers: {
            baselayers: {
                mapbox_light: {
                    name: 'MapBox Light',
                    url: ''
                }
            }
        },
        paths: []
    }

    function featureSelect(feature) {
        ctrl.map.paths = {};
        ctrl.map.paths[feature.type] = ctrl.convertSpatial(feature);
        ctrl.highway.shape = feature;
    }

    function convertSpatial(feature) {
        console.log("Converting spatial feature:");
        var type = "polyline";
        var pathNew = {
            type: type,
            weight: 3,
            latlngs: new Array()
        };

        if (!!feature.lines) {
            pathNew.type = "multiPolyline";
            console.log("...MultiLine");
            for (var l = 0; l < feature.lines.length; l++) {
                pathNew.latlngs.push(new Array());
                var line = feature.lines[l];
                for (var i = 0; i < line.points.length; i++) {
                    pathNew.latlngs[l].push({
                        lat: line.points[i].lat,
                        lng: line.points[i].lng
                    });
                }
            }
        } else {
            console.log("...Line");
            for (var p = 0; p < feature.points.length; p++) {
                pathNew.latlngs.push({
                    lat: feature.points[p].lat,
                    lng: feature.points[p].lng
                });
            }
        }
        console.log("...Done");

        return pathNew;
    }
    //Pegando os dados do ID
   
    this.shapeFile = {};

    function upload(file) {
        Upload.upload({
            url: settings.sgaApiBaseUrl + "gis/readshp",
            data: { file: file }
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded.');//' Response: ' + resp.data);
            ctrl.shapeFile = resp.data;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });

        this.validation = {
            numero: 1
        }
    };

};


angular.module('SGA').factory('Highway', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highway/:id', null, {
        'put': { method: 'PUT' },
        'query':{method: 'GET'}
    });
}]);
angular.module('SGA').controller('HighwaySearchController', [
    '$http',
    'searchService',
    'Highway',
    'settings',
    'Upload',
    '$scope',
    '$stateParams',


    function ($http,
                        searchService,
                        Highway,
                        settings,
                        Upload,
                        $scope,
                        $stateParams

                        ) {

        var ctrl = this;
        ctrl.search = searchService.Instance('highway');
    }]);
angular.module('SGA').factory('Shape', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'gis/readshp/');

    }]);
angular.module('SGA').controller('HighwayAppController', [
    '$http',
    '$sce',
    'searchService',
    'settings',
    '$scope',
    '$stateParams',
    'appService',
    function ($http,
    $sce,
    searchService,
    settings,
    $scope,
    $stateParams,
    appService) {
        var ctrl = this;
        ctrl.view = 0;
        ctrl.markers = [];
        ctrl.accessRoad = appService.get.query(null, function () {            
            var i = 0;
            while (i < ctrl.accessRoad.length) {
                if (ctrl.markers.length == ctrl.accessRoad.length) {
                    break;
                }
                ctrl.markers.push({
                    idaccessRoad: ctrl.accessRoad[i].idaccessRoad,
                    lat: ctrl.accessRoad[i].position.points[0].lat,
                    lng: ctrl.accessRoad[i].position.points[0].lng,
                })

                i++;
            }
        });
    }]);
angular.module('SGA').controller('HighWayAccessRoadController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , 'leafletBoundsHelpers'
                 , 'leafletData'
                 , 'HighwayAccessRoads'
                 , 'Programs'
                 , 'dialogService'
                 , 'messageService'
                 , 'RoadSides'
                 , 'HighwayWorkSites'
                 , 'Directions'
                 , 'HighwaySections'
                 , 'Elements'
                 , 'Upload'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams'
                 , 'JudicialProcessStatus'
                 , 'NotificationType',

        function ($timeout, $q, searchService, settings, leafletBoundsHelpers, leafletData, HighwayAccessRoads, Programs, dialogService, messageService, RoadSides, HighwayWorkSites, Directions, HighwaySections, Elements, Upload, $scope, $filter, $state, $stateParams, JudicialProcessStatus, NotificationType) {

            var ctrl = this;

            this.showWeeks = false; //Remover barra lateral do datepicker

            this.accessTypes = [{ idaccessType: 1, name: "Comercial" }, { idaccessType: 2, name: "Industrial" }, { idaccessType: 3, name: "Residencial" }, { idaccessType: 4, name: "Lavoura" }, { idaccessType: 5, name: "Público" }, { idaccessType: 6, name: "Outros" }];

            this.pavementTypes = [{ idpavementType: 1, name: "Flexível" }, { idpavementType: 2, name: "Semi-Rígido" }, { idpavementType: 3, name: "Rígido" }, { idpavementType: 4, name: "Sem Revestimento" }];

            this.openCalendar = function (inspection) {
                //$event.preventDefault();
                //$event.stopPropagation();

                inspection.isOpen = true;
            };

            /* Map vars and functions*/
            this.mapMarkers = new Array();
            this.updateMap = function () {
                var bounds = L.latLngBounds(ctrl.mapMarkers);
                if (bounds.isValid()) {
                    ctrl.center = bounds.getCenter();
                    leafletData.getMap().then(function (map) {
                        map.fitBounds(bounds.pad(0.5));
                        if (ctrl.mapMarkers.length === 1) {
                            map.setZoom(12);
                        }
                    });
                }
            }

            ctrl.selectedHighwaySection = null;

            $scope.$watch('ctrl.accessRoad.idhighwaySection', function () {
                ctrl.selectedHighwaySection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.accessRoad.idhighwaySection }, true)[0];
                ctrl.updateDirectionCollection();

            });

            $scope.$watchCollection(function () {
                return ctrl.highwaySections;
            }, function () {
                ctrl.updateDirectionCollection();
            });


            //Collections that are used to fill in the UI elements
            this.contracts = [{ "name": "EE-53", "idcontract": 6 }];

            this.cities = [
                { idterritorialunit: 9895, name: "Areal/RJ" },
                { idterritorialunit: 9897, name: "Comendador Levy Gasparian/RJ" },
                { idterritorialunit: 9911, name: "Três Rios/RJ" },
                { idterritorialunit: 10121, name: "Petrópolis/RJ" },
                { idterritorialunit: 10163, name: "Duque de Caxias/RJ" },
                { idterritorialunit: 10231, name: "Rio de Janeiro/RJ" },
                { idterritorialunit: 9242, name: "Juiz de Fora/MG" },
                { idterritorialunit: 9274, name: "Matias Barbosa/MG" },
                { idterritorialunit: 9323, name: "Simão Pereira/MG" }

            ];

            this.highwaySections = [];
            this.directions = [];
            this.roadsides = RoadSides.query(); //get all roadsides (generic to all contracts, no need to filter here)
            this.judicialProcessStatusLookup = JudicialProcessStatus.query();
            this.notificationTypeLookup = NotificationType.query();

            //Updates the direction colection and also sets the selected section
            //TODO: Rename this function to reflect its actual function
            this.updateDirectionCollection = function () {
                var arr = [];

                if (ctrl.highwaySections === null || ctrl.highwaySections.length === 0) {

                } else {
                    if (ctrl.accessRoad.idhighwaySection === null) {
                        angular.forEach(ctrl.highwaySections, function (key, value) {
                            $.extend(true, arr, key.directions);
                        });
                        ctrl.directions = arr;
                    } else {
                        var selectedSection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.accessRoad.idhighwaySection }, true);
                        ctrl.directions = selectedSection[0].directions;
                    }
                }
            }

            //Function that gets all necessary location information based on the filter (km, highway and contract)
            this.updateLocationFields = function () {
                console.log("Update location fields");
                var self = this;

                //Passar o Code para o breadcump  
                this.code = ctrl.accessRoad.code;

                //set all variables to zero if no position was chosen
                if (!ctrl.accessRoad.positionStart) {
                    ctrl.directions = [];
                    ctrl.highwaySections = [];
                    ctrl.accessRoad.iddirection = null;
                    ctrl.selectedHighwaySection = null;
                    ctrl.accessRoad.idhighwaySection = null;
                    return;
                }

                //Define the lookup parameters
                var par = {
                    idhighway: ctrl.idhighway,
                    kmfilter: ctrl.accessRoad.positionStart,
                    icontract: ctrl.accessRoad.idcontract
                }

                if (this.wsSearchPending) {
                    $timeout.cancel(this.wsSearchPending);
                }

                //Function that updates the list of Worksites (depends on accessRoad.km)
                //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
                this.wsSearchPending = $timeout(
                    function () {
                        console.log("Getting sections");
                        self.highwaySections = HighwaySections.query(par, function () {
                        });

                    }, settings.autoCompleteDelay); //Configurable delay used here
            };

            this.accessRoad = HighwayAccessRoads.get({ id: $stateParams.id || 0 },
                function (o) {
                    console.log("HighwayAccessRoad loaded");
                    ctrl.updateLocationFields();
                    ctrl.accessRoad.idcontract = ctrl.contracts[0].idcontract;

                    //transform coordinate in to array. Required by Leaflet
                    ctrl.accessRoad.position = ctrl.accessRoad.position ? ctrl.accessRoad.position : { points: [] };
                    var coord = (ctrl.accessRoad.position && ctrl.accessRoad.position.points) ? ctrl.accessRoad.position.points : [];
                    coord.forEach(function (e, i, a) {
                        ctrl.mapMarkers.push({ lat: e.lat, lng: e.lng });
                    });

                    //transform the dates of the actions (needed for validation)
                    ctrl.accessRoad.actions.forEach(function (e, i, a) {
                        e.performedOn = new Date(e.performedOn);
                    });


                    ctrl.updateMap();

                });
            
            this.save = function() {
                ctrl.accessRoad.$save(
                  function (x, y) {
                      var title = 'Salvo';
                      var message = 'Arquivo salvo com sucesso';
                      messageService.send(message, title);
                      $state.go('highwayaccessroads.id', { id: ctrl.accessRoad.idaccessRoad }, { reload: true });
                  },
                    function () {
                        var title = 'Problema';
                        var message = 'Problema ao salvar, contate o administrador';
                        messageService.send(message, title);
                    });
            }


            this.delete = function () {
               HighwayAccessRoads.delete({ id: $stateParams.id },
              function () {
                  var title = 'Apagado';
                  var message = 'Arquivo apagado com sucesso';
                  messageService.send(message, title);
                  $state.go('highwayaccessroads');
              },
                function () {
                    var title = 'Problema';
                    var message = 'Problema ao apagar, contate o administrador';
                    messageService.send(message, title);
                });
            }

            this.cancel = function() {
                $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
            }

            this.addInspection = function () {
                ctrl.accessRoad.actions = ctrl.accessRoad.actions || [];
                var i = ctrl.accessRoad.actions;
                i.push({
                    performedOn: new Date(moment().format()),
                    description: "",
                    state: 'Added'
                });

            }

            this.deleteInspection = function (action) {
                action.state = 'Deleted';
            }

            this.markInspectionAsChanged = function (action) {
                if (action.state !== 'Added') {
                    action.state = 'Modified';
                }
            }

            this.checkValids = function () {
                console.log($scope.validation.$error.required);
            }

        }]);


angular.module('SGA').factory('HighwayAccessRoads', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highwayaccessroad/:id');

}]);





angular.module('SGA').controller('HighWayAccessRoadSearchController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams',

        function ($timeout, $q, searchService, settings, $scope, $filter, $state, $stateParams) {

            var ctrl = this;

            ctrl.search = searchService.Instance('highwayaccessroad');
        }]);
angular.module('SGA').controller('RoadsideOccupationController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , 'leafletBoundsHelpers'
                 , 'leafletData'
                 , 'RoadsideOccupations'
                 , 'Programs'
                 , 'dialogService'
                 , 'messageService'
                 , 'RoadSides'
                 , 'HighwayWorkSites'
                 , 'Directions'
                 , 'HighwaySections'
                 , 'Elements'
                 , 'Upload'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams'
                 , 'JudicialProcessStatus'
                 , 'NotificationType'
                 ,

        function ($timeout, $q, searchService, settings, leafletBoundsHelpers, leafletData, RoadsideOccupations, Programs, dialogService, messageService, RoadSides, HighwayWorkSites, Directions, HighwaySections, Elements, Upload, $scope, $filter, $state, $stateParams, JudicialProcessStatus, NotificationType) {

            var ctrl = this;

            this.showWeeks = false; //Remover barra lateral do datepicker

            this.id = $stateParams.id;

            //this.search = searchService.Instance('roadsideoccupation');

            this.judicialProcessDistributionDateIsOpen = {};

            this.landUses = [{ idlandUse: 1, name: "Comercial" }, { idlandUse: 2, name: "Industrial" }, { idlandUse: 3, name: "Residencial" }, { idlandUse: 4, name: "Lavoura" }, { idlandUse: 5, name: "Público" }, { idlandUse: 6, name: "Passivo Ambiental" }, { idlandUse: 7, name: "Outros" }];
            //this.pavementTypes = [{ idpavementType: 1, name: "Flexível" }, { idpavementType: 2, name: "Semi-Rígido" }, { idpavementType: 3, name: "Rígido" }, { idpavementType: 4, name: "Sem Revestimento" }];
            this.domainSituations = [{ iddomainSituation: 1, name: "Faixa de Domínio" }, { iddomainSituation: 2, name: "Faixa Não Edificante" }, { iddomainSituation: 3, name: "Faixa de Domínio e Não Edificante" }];
            this.roadObjects = [{ idroadsideObject: 1, name: "Telecomunicação" }, { idroadsideObject: 2, name: "Ponto de Ônibus" }, { idroadsideObject: 3, name: "Elétrica" }, { idroadsideObject: 4, name: "Painel" }, { idroadsideObject: 5, name: "Edificação" }, { idroadsideObject: 6, name: "Outros" }];
            this.openCalendar = function (cal) {
                cal.isOpen = true;
            };

            /* Map vars and functions*/
            this.mapMarkers = new Array();
            this.updateMap = function () {
                var bounds = L.latLngBounds(ctrl.mapMarkers);
                if (bounds.isValid()) {
                    ctrl.center = bounds.getCenter();
                    leafletData.getMap().then(function (map) {
                        map.fitBounds(bounds.pad(0.5));
                        if (ctrl.mapMarkers.length === 1) {
                            map.setZoom(12);
                        }
                    });
                }
            }

            ctrl.selectedHighwaySection = null;

            $scope.$watch('ctrl.roadsideOccupation.idhighwaySection', function () {
                ctrl.selectedHighwaySection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.roadsideOccupation.idhighwaySection }, true)[0];
                ctrl.updateDirectionCollection();

            });

            $scope.$watchCollection(function () {
                return ctrl.highwaySections;
            }, function () {
                ctrl.updateDirectionCollection();
            });


            //Collections that are used to fill in the UI elements
            this.contracts = [{ "name": "EE-53", "idcontract": 6 }];

            this.cities = [
                { idterritorialunit: 9895, name: "Areal/RJ" },
                { idterritorialunit: 9897, name: "Comendador Levy Gasparian/RJ" },
                { idterritorialunit: 9911, name: "Três Rios/RJ" },
                { idterritorialunit: 10121, name: "Petrópolis/RJ" },
                { idterritorialunit: 10163, name: "Duque de Caxias/RJ" },
                { idterritorialunit: 10231, name: "Rio de Janeiro/RJ" },
                { idterritorialunit: 9242, name: "Juiz de Fora/MG" },
                { idterritorialunit: 9274, name: "Matias Barbosa/MG" },
                { idterritorialunit: 9323, name: "Simão Pereira/MG" }

            ];
           
            this.highwaySections = [];
            this.directions = [];
            this.roadsides = RoadSides.query(); //get all roadsides (generic to all contracts, no need to filter here)

            this.judicialProcessStatusLookup = JudicialProcessStatus.query();

            this.notificationTypeLookup = NotificationType.query();

            //Updates the direction colection and also sets the selected section
            //TODO: Rename this function to reflect its actual function
            this.updateDirectionCollection = function () {
                var arr = [];

                if (ctrl.highwaySections === null || ctrl.highwaySections.length === 0) {

                } else {
                    if (ctrl.roadsideOccupation.idhighwaySection === null) {
                        angular.forEach(ctrl.highwaySections, function (key, value) {
                            $.extend(true, arr, key.directions);
                        });
                        ctrl.directions = arr;
                    } else {
                        var selectedSection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.roadsideOccupation.idhighwaySection }, true);
                        ctrl.directions = selectedSection[0].directions;
                    }
                }
            }

            //Function that gets all necessary location information based on the filter (km, highway and contract)
            this.updateLocationFields = function () {
                console.log("Update location fields");
                var self = this;

                //Passar o Code para o breadcump  
                this.code = ctrl.roadsideOccupation.code;                

                //set all variables to zero if no position was chosen
                if (!ctrl.roadsideOccupation.positionStart) {
                    ctrl.directions = [];
                    ctrl.highwaySections = [];
                    ctrl.roadsideOccupation.iddirection = null;
                    ctrl.selectedHighwaySection = null;
                    ctrl.roadsideOccupation.idhighwaySection = null;
                    return;
                }

                //Define the lookup parameters
                var par = {
                    idhighway: ctrl.idhighway,
                    kmfilter: ctrl.roadsideOccupation.positionStart,
                    icontract: ctrl.roadsideOccupation.idcontract
                }

                if (this.wsSearchPending) {
                    $timeout.cancel(this.wsSearchPending);
                }

                //Function that updates the list of Worksites (depends on roadsideOccupation.km)
                //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
                this.wsSearchPending = $timeout(
                    function () {
                        console.log("Getting sections");
                        self.highwaySections = HighwaySections.query(par, function () {
                        });

                    }, settings.autoCompleteDelay); //Configurable delay used here
            };

            this.roadsideOccupation = RoadsideOccupations.get({ id: $stateParams.id || 0 },

                function (o) {
                    console.log("HighwayAccessRoad loaded");

                    ctrl.updateLocationFields();
                    ctrl.roadsideOccupation.idcontract = ctrl.contracts[0].idcontract;
                    
                    //transform coordinate in to array. Required by Leaflet
                    ctrl.roadsideOccupation.position = ctrl.roadsideOccupation.position ? ctrl.roadsideOccupation.position : { points: [] };
                    var coord = (ctrl.roadsideOccupation.position && ctrl.roadsideOccupation.position.points) ? ctrl.roadsideOccupation.position.points : [];
                    coord.forEach(function (e, i, a) {
                        ctrl.mapMarkers.push({ lat: e.lat, lng: e.lng });
                    });

                    //transform the dates of the actions (needed for validation)
                    ctrl.roadsideOccupation.actions.forEach(function (e, i, a) {
                        e.performedOn = new Date(e.performedOn);
                    });

                    //transform the other dates on the form
                    ctrl.roadsideOccupation
                        .judicialProcessDistributionDate = new
                        Date(ctrl.roadsideOccupation.judicialProcessDistributionDate);
                    
                    ctrl.updateMap();
                   

                });
       
            
            this.save = function () {
                ctrl.roadsideOccupation.$save(
                    function (x, y) {
                        var title = 'Salvo';
                        var message = 'Item salvo com sucesso';
                        messageService.send(message, title);
                        $state.go('roadsideoccupations.id', { id: ctrl.roadsideOccupation.idroadsideOccupation }, { reload: true });
                    },
                    function () {
                        var title = 'Problema';
                        var message = 'Problema ao salvar, contate o administrador';
                        messageService.send(message, title);
                    });
            }


            this.delete = function () {
               RoadsideOccupations.delete({ id: $stateParams.id },
               function () {
                   var title = 'Apagado';
                   var message = 'Item apagado com sucesso';
                   messageService.send(message, title);
                   $state.go('roadsideoccupations');
               },
                function () {
                    var title = 'Problema';
                    var message = 'Problema ao apagar, contate o administrador';
                    messageService.send(message, title);
                });
            }

            this.cancel = function() {
                $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
            }

            this.addInspection = function () {
                ctrl.roadsideOccupation.actions = ctrl.roadsideOccupation.actions || [];
                var i = ctrl.roadsideOccupation.actions;
                i.push({
                    performedOn: new Date(moment().format()),
                    description: "",
                    state: 'Added'
                });

            }

            this.deleteInspection = function (action) {
                action.state = 'Deleted';
            }

            this.markInspectionAsChanged = function (action) {
                if (action.state !== 'Added') {
                    action.state = 'Modified';
                }
            }

            this.checkValids = function () {
                console.log($scope.validation.$error.required);
            }

        }]);


angular.module('SGA').factory('RoadsideOccupations', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'roadsideoccupation/:id');

    
}]);

angular.module('SGA').factory('JudicialProcessStatus', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'judicialprocessstatus/:id');
    
}]);

angular.module('SGA').factory('NotificationType', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'notificationtype/:id');

}]);

//angular.module('SGA').controller('IncidentInspection', ['$modalInstance', 'inspection', function ($modalInstance, inspection) {
//    this.inspection = inspection;

//    this.ok = function () {
//        $modalInstance.close('ok');
//    };

//    this.cancel = function () {
//        $modalInstance.dismiss('cancel');
//    };
//}]);



angular.module('SGA').controller('RoadsideOccupationSearchController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams'
                 ,

        function ($timeout, $q, searchService, settings, $scope, $filter, $state, $stateParams) {

            var ctrl = this;

            ctrl.search = searchService.Instance('roadsideoccupation');
        }]);
angular.module('SGA').factory('Elements', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'element/:id');

}]);

angular.module('SGA').controller('IncidentController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , 'leafletBoundsHelpers'
                 , 'leafletData'
                 , 'Incidents'
                 , 'dialogService'
                 , 'messageService'
                 , 'Programs'
                 , 'RoadSides'
                 , 'HighwayWorkSites'
                 , 'Directions'
                 , 'HighwaySections'
                 , 'Elements'
                 , 'Upload'
                 , '$scope'
                 , '$filter'
                 , '$stateParams',
        function ($timeout, $q, searchService, settings, leafletBoundsHelpers, leafletData, Incidents, dialogService, messageService, Programs, RoadSides, HighwayWorkSites, Directions, HighwaySections, Elements, Upload, $scope, $filter, $stateParams) {

            var ctrl = this;

            //this.search = searchService.Instance('incident');


            /* Map vars and functions*/
            this.mapMarkers = new Array();
            this.updateMap = function () {
                var bounds = L.latLngBounds(ctrl.mapMarkers);
                if (bounds.isValid()) {
                    ctrl.center = bounds.getCenter();
                    leafletData.getMap().then(function (map) {
                        map.fitBounds(bounds.pad(0.5));
                    });
                }
            }

            ctrl.selectedHighwaySection = null;

            $scope.$watch('ctrl.incident.idhighwaySection', function () {
                ctrl.selectedHighwaySection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.incident.idhighwaySection }, true)[0];
                ctrl.updateDirectionCollection();

            });

            $scope.$watchCollection(function () {
                return ctrl.highwaySections;
            }, function () {
                ctrl.updateDirectionCollection();
            });


            //Collections that are used to fill in the UI elements
            this.contracts = [{ "name": "EE-53", "idcontract": 6 }];

            this.highwaySections = [];
            this.worksites = [];
            this.directions = [];
            this.programs = Programs.query(); //get all programs //TODO: Filter by contract
            this.roadsides = RoadSides.query(); //get all roadsides (generic to all contracts, no need to filter here)
            this.elements = Elements.query(); // Get all elements. At the moment, these are the same for all projects, no need to filter
            this.loadElements = function (query) { // the tags directive uses a promise for autocomplete. This function provides that
                var deferred = $q.defer();
                deferred.resolve(ctrl.elements);
                return deferred.promise;
            };

            //Updates the direction colection and also sets the selected section
            //TODO: Rename this function to reflect its actual function
            this.updateDirectionCollection = function () {
                var arr = [];

                if (ctrl.highwaySections === null || ctrl.highwaySections.length === 0) {

                } else {
                    if (ctrl.incident.idhighwaySection === null) {
                        angular.forEach(ctrl.highwaySections, function (key, value) {
                            $.extend(true, arr, key.directions);
                        });
                        ctrl.directions = arr;
                    } else {
                        var selectedSection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.incident.idhighwaySection }, true);
                        ctrl.directions = selectedSection[0].directions;
                    }
                }
            }

            //Function that gets all necessary location information based on the filter (km, highway and contract)
            this.updateLocationFields = function () {
                console.log("Update location fields");
                var self = this;

                //Passar o Code para o breadcrumb
                this.code = ctrl.incident.code;

                //set all variables to zero if no position was chosen
                if (!ctrl.incident.positionStart) {
                    ctrl.worksites = []; //away
                    ctrl.idhighwayWorkSite = null; //away
                    ctrl.directions = [];
                    ctrl.highwaySections = [];
                    ctrl.incident.iddirection = null;
                    ctrl.incident.idhighwayWorkSite = null;
                    ctrl.selectedHighwaySection = null;
                    ctrl.incident.idhighwaySection = null;
                    return;
                }

                //Define the lookup parameters
                var par = {
                    idhighway: ctrl.idhighway,
                    kmfilter: ctrl.incident.positionStart,
                    icontract: ctrl.incident.idcontract
                }

                if (this.wsSearchPending) {
                    $timeout.cancel(this.wsSearchPending);
                }

                //Function that updates the list of Worksites (depends on incident.km)
                //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
                this.wsSearchPending = $timeout(
                    function () {
                        console.log("Getting sections");
                        self.highwaySections = HighwaySections.query(par, function () {
                        });

                    }, settings.autoCompleteDelay); //Configurable delay used here
            };

            //this.updateWorkSites = function () {
            //    console.log("update ws");

            //    //In case the starting position is null, clear the worksite collection and the current worksite. Don't do anything after that 
            //    if (ctrl.incident.positionStart == "") {
            //        ctrl.worksites = [];
            //        ctrl.idhighwayWorkSite = null;
            //        ctrl.directions = [];
            //        ctrl.incident.iddirection = null;
            //        return;
            //    }

            //    var par = { kmfilter: ctrl.incident.positionStart };

            //    if (this.wsSearchPending) {
            //        $timeout.cancel(this.wsSearchPending);
            //    }

            //    //Function that updates the list of Worksites (depends on incident.km)
            //    //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
            //    this.wsSearchPending = $timeout(
            //        function () {
            //            console.log("getting ws");
            //            self.worksites = HighwayWorkSites.query(par, function () {
            //                if (self.worksites.length === 1) {
            //                    self.incident.idhighwayWorkSite = self.worksites[0].idhighwayWorkSite;
            //                    ctrl.directions = Directions.query({ idhighwayWorkSiteFilter: ctrl.incident.idhighwayWorkSite }, function (d) { console.log("Directions Loaded"); });
            //                } else if (self.worksites.length === 0) {
            //                    self.incident.idhighwayWorkSite = null;
            //                    ctrl.directions = [];
            //                    ctrl.incident.iddirection = null;
            //                }
            //            });

            //        }, settings.autoCompleteDelay); //Configurable delay used here
            //};

            //AFTER the incident is returned from the server, load the worksites and after that, the directions as well
            this.incident = Incidents.get({ id: $stateParams.id || 0 },

                function (o) {
                    console.log("Incident loaded");
                    ctrl.updateLocationFields();
                    ctrl.incident.idcontract = ctrl.contracts[0].idcontract;
                    //ctrl.worksites = ctrl.incident && ctrl.incident.state == 'New' ? [] : HighwayWorkSites.query({ km: ctrl.incident.start }, function (h) {
                    //    console.log("Worksites loaded");
                    //    ctrl.directions = Directions.query({ idhighwayWorkSiteFilter: ctrl.incident.idhighwayWorkSite }, function (d) { console.log("Directions Loaded"); });
                    //});

                    //transform coordinate in to array. Required by Leaflet
                    var coord = (ctrl.incident.coordinate && ctrl.incident.coordinate.points) ? ctrl.incident.coordinate.points : [];
                    coord.forEach(function (e, i, a) {
                        ctrl.mapMarkers.push({ lat: e.lat, lng: e.lng });
                    });

                    ctrl.updateMap();

                });

            this.save = function () {
                //ctrl.incident.$save();// Incidents.save(this.incident, null, function () { alert('Ocorrência gravada com sucesso') }, function () { alert('Problema ao gravar a ocorrência') });
                ctrl.incident.$save(
                    function (x, y) {
                        var title = 'Salvo';
                        var message = 'Arquivo salvo com sucesso';
                        messageService.send(message, title);
                    },
                    function () {
                        var title = 'Problema';
                        var message = 'Problema ao salvar, contate o administrador';
                        messageService.send(message, title);
                    });

                //, function(x,y) {alert('Problema em gravar os dados');
            }


            this.delete = function () {
                Incidents.delete({ id: $stateParams.id },
               function () {
                   var title = 'Apagado';
                   var message = 'Arquivo apagado com sucesso';
                   messageService.send(message, title);
               },
                function () {
                    var title = 'Problema';
                    var message = 'Problema ao apagar, contate o administrador';
                    messageService.send(message, title);
                });
            }


            this.addInspection = function () {
                var i = ctrl.incident.inspections;
                if (i.length === 0) {
                    i.push({
                        number: 1,
                        date: moment().format(),
                        comment: "-",
                        state: 'Added',
                        photos: [],
                        idstatus: 2
                    });
                } else {
                    i.push({
                        number: i[i.length - 1].number + 1,
                        date: moment().format(),
                        comment: "-",
                        state: 'Added',
                        photos: [],
                        idstatus: 1
                    });
                }
            }

            this.deleteInspection = function (inspection) {
                inspection.state = 'Deleted';
            }

            this.markInspectionAsChanged = function (inspection) {
                if (inspection.state !== 'Added') {
                    inspection.state = 'Modified';
                }
            }

        }]);


angular.module('SGA').factory('Incidents', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'incident/:id', null, {
        'delete': { method: 'DELETE' },
        'contracts': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'incidents/contracts' },
    });


    //TODO definir os metodos de gerenciamento de: elementos; vistorias e suas fotos

}]);

//angular.module('SGA').controller('IncidentInspection', ['$modalInstance', 'inspection', function ($modalInstance, inspection) {
//    this.inspection = inspection;

//    this.ok = function () {
//        $modalInstance.close('ok');
//    };

//    this.cancel = function () {
//        $modalInstance.dismiss('cancel');
//    };
//}]);



angular.module('SGA').controller('IncidentSearchController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , '$scope'
                 , '$filter'
                 , '$stateParams',
        function ($timeout, $q, searchService, settings, $scope, $filter, $stateParams) {

            var ctrl = this;

            ctrl.search = searchService.Instance('incident');
        }]);
angular.module('SGA').controller('ProgramController', [
        '$http', '$stateParams', 'searchService', 'Programs', function ($http, $stateParams, searchService, Programs) {

            this.search = searchService.Instance('program');
            this.program = Programs.get({ id: $stateParams.id });

        }]);



angular.module('SGA').factory('Programs', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'program/:id');

}]);
angular.module('SGA').controller('DirectionController', [
        '$http', '$stateParams', 'searchService', 'Directions', function ($http, $stateParams, searchService, Directions) {

            this.search = searchService.Instance('direction');
            this.direction = Directions.get({ id: $stateParams.id });

        }]);

angular.module('SGA').factory('Directions', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'direction/:id');

}]);
angular.module('SGA').controller('ProgramController', [
        '$http', '$stateParams', 'searchService', 'Programs','HighwayWorkSites', function ($http, $stateParams, searchService, HighwayWorkSites) {

            this.search = searchService.Instance('highwayworksite');
            this.highwayWorkSite = HighwayWorkSites.get({ id: $stateParams.id });

        }]);



angular.module('SGA').factory('HighwayWorkSites', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highwayworksite/:id');

}]);


angular.module('SGA').factory('HighwaySections', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highwaysection/:id');

}]);
angular.module('SGA').controller('RoadSideController', [
        '$http', '$stateParams', 'searchService', 'RoadSides', function ($http, $stateParams, searchService, RoadSides) {

            this.search = searchService.Instance('roadside');
            this.roadside = RoadSides.get({ id: $stateParams.id });

        }]);



angular.module('SGA').factory('RoadSides', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'roadside/:id');

}]);
(function () {
    "use strict";

    angular
        .module("SGA")
        .controller("LoginController",
                    ["userAccount",
                        "currentUser",
                        "$state",
                        loginController]);

    function loginController(userAccount, currentUser, $state) {
        var vm = this;

        //vm.checkLoggin = function () {
        //    if (!currentUser.getProfile().isLoggedIn) {
        //        $state.go('login');
        //    } else {
        //        return currentUser.getProfile().isLoggedIn;
        //    }
        //}


        vm.isLoggedIn = function () {
            return currentUser.getProfile().isLoggedIn;
        };
        vm.logout = function () {
            currentUser.clearProfile();
            $state.go('login', {}, { reload: true });

        }

        vm.currentUser = currentUser.getProfile().username;
        vm.login = function () {
            vm.userData.grant_type = "password";
            vm.userData.userName = vm.userData.email;
            userAccount.login.loginUser(vm.userData,
                function (data) {
                    vm.message = "";
                    vm.password = "";
                    currentUser.setProfile(vm.userData.userName, data.access_token);
                    $state.go('home', {}, { reload: true });
                },
                function (response) {
                    vm.password = "";
                    if (response.data.error === "invalid_grant") {
                        //vm.message = treatmentService.log;
                        //vm.message = "Usuário e/ou senha incorreto!";
                        
                    }
                });

        }
    }
})();
angular.module('SGA').controller('MenuController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    'menuService',
    '$stateParams',
    function (
        $http,
        $resource,
        searchService,
        settings,
        menuService,
        $stateParams
        ) {
        var ctrl = this;

        ////Pegar todo o Menu
        this.index = menuService.menu.query();
        $(function() {
            $('[data-toggle="offcanvas"]').click(function(e) {
                e.preventDefault();
                var navside = '.nav-side-menu',
                $navsidetip = $('[data-toggle="tooltip-menu"]');
                $(navside + ', .content-wrapper').toggleClass('toggled');
                if ($(navside).hasClass('toggled')) {
                    $navsidetip.tooltip({
                        container: 'body'
                    });
                } else {
                    $navsidetip.tooltip('destroy');
                }
            });
        })
    }
]);
angular.module('SGA').controller('MapController', [
    '$http',
    '$sce',
    'searchService',
    'settings',
    '$scope',
    '$stateParams',
    'appService',
    'mapService',
    'leafletMarkerEvents',
    function (
        $http,
        $sce,
        searchService,
        settings,
        $scope,
        $stateParams,
        appService,
        mapService,
        leafletMarkerEvents) {
        var ctrl = this;
        ctrl.search = searchService.Instance($stateParams.entityName);
        $scope.$on('leafletDirectiveMarker.main.click', function (e, args) {
            ctrl.marker = mapService.mapArea.get({ entityName: $stateParams.entityName , id: args.model.id || 0 });
        })
    }
])
angular.module('SGA').controller('ReportController', [
    '$http',
    '$resource',
    'searchService',
    'settings', 
    'reportService',
    '$stateParams',

    function (
        $http,
        $resource,
        searchService,
        settings,
        reportService,
        $stateParams
                     ) {
        var ctrl = this;

        this.currentreport = null;
        this.contrato = null;

        //Lista todos os contratos
        this.prefix = reportService.prefix.query();

        //Foi criado esta função para que quando ela fosse chamada, a mesma retornaria todos os relatórios atrelados ao contrato
        this.busca = function () {
            cursor_wait();
            this.reportgroups = reportService.list.query({module:$stateParams.module, idcontract: this.contrato }, function (result) {
                remover_cursor();
            })
        };
        //Função que verifica se existe algum dado dentro do objeto parametro 
        this.obj = function (data) {
            for (var i in data) if (data.hasOwnProperty(i)) return false;
            return true;
        }
        //Função do download no formato Excel
        this.excel = function (list) {
            this.reportlist = list;
            cursor_wait();
            //console.log(this.reportlist);
            //Envia as informações do relatorio para o servidor e espera a chave para fazer o download 
            var uid = reportService.renderExcel.save(this.reportlist,
                function () {
                    //Solicita ao servidor o download da chave passado pelo UID
                    window.location = settings.sgaApiBaseUrl + 'reports/download?fileName=' + uid.fileName + '&key=' + uid.key;
                    remover_cursor();
                });
        }
        //Função do download no formato PDF
        this.pdf = function (list) {
            this.reportlist = list;
            cursor_wait();
            //console.log(this.reportlist);
            var uid = reportService.renderPdf.save(this.reportlist,
                function () {
                    window.location = settings.sgaApiBaseUrl + 'reports/download?fileName=' + uid.fileName + '&key=' + uid.key;
                    remover_cursor();
                });
        }
        //Função para mudar o simbolo do  cursor
        function cursor_wait() {
            // switch to cursor wait for the current element over
            var elements = $(':hover');
            if (elements.length) {
                // get the last element which is the one on top
                elements.last().addClass('cursor-wait');
            }
            // add class to use it in the mouseover selector (to avoid conflicts)
            $('html, body').addClass('cursor-wait');
            // switch to cursor wait for all elements you'll be over
            $('html').on('mouseover', 'body.cursor-wait*', function (e) {
                $(e.target).addClass('cursor-wait');
            });
        }
        function remover_cursor() {
            $('html').off('mouseover', 'body.cursor-wait *'); // remove event handler
            $('.cursor-wait').removeClass('cursor-wait'); // get back to default
        }


    }
]);

angular.module('SGA').controller('treeFaunaController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    '$stateParams',
    'loadingService',
    function ($http, $resource, searchService, settings, stateParams, loadingService) {

        var ctrl = this;
        ctrl.id = null;

        ctrl.my_tree_handler = function (branch) {
            var _ref;
            ctrl.output = "You selected: " + branch.label;
            if ((_ref = branch.data) !== null ? _ref.description : void 0) {
                return ctrl.output += '(' + branch.data.description + ')';
            }
        }
        ctrl.apple_selected = function (branch) {
            return ctrl.output = "APPLE! : " + branch.label;
        };
        ctrl.my_tree = {};

        //  ctrl.nodes = [
        //{
        //    label: 'Animal',
        //    children: [
        //      {
        //          label: 'Dog',
        //          data: {
        //              description: "man's best friend"
        //          }
        //      }, {
        //          label: 'Cat',
        //          data: {
        //              description: "Felis catus"
        //          }
        //      }, {
        //          label: 'Hippopotamus',
        //          data: {
        //              description: "hungry, hungry"
        //          }
        //      }, {
        //          label: 'Chicken',
        //          children: ['White Leghorn', 'Rhode Island Red', 'Jersey Giant']
        //      }
        //    ]
        //},
        //{
        //    label: 'Vegetable',
        //    data: {
        //        definition: "A plant or part of a plant used as food, typically as accompaniment to meat or fish, such as a cabbage, potato, carrot, or bean.",
        //        data_can_contain_anything: true
        //    },
        //    onSelect: function (branch) {
        //        return ctrl.output = "Vegetable: " + branch.data.definition;
        //    },
        //    children: [
        //      {
        //          label: 'Oranges'
        //      }, {
        //          label: 'Apples',
        //          children: [
        //            {
        //                label: 'Granny Smith',
        //                onSelect: ctrl.apple_selected,
        //                children: [
        //            {
        //                label: 'Granny Smith com nome muuuuito maiiiior',
        //                onSelect: ctrl.apple_selected
        //            }, {
        //                label: 'Red Delicous',
        //                onSelect: ctrl.apple_selected
        //            }, {
        //                label: 'Fuji',
        //                onSelect: ctrl.apple_selected
        //            }
        //                ]
        //            }, {
        //                label: 'Red Delicous',
        //                onSelect: ctrl.apple_selected
        //            }, {
        //                label: 'Fuji',
        //                onSelect: ctrl.apple_selected
        //            }
        //          ]
        //      }
        //    ]
        //},

        //  ]
        ctrl.nodes = [];
       
        //Pegas as informações no backend
        $http.get("http://localhost:10000/api/TerritorialUnit").then(
            function (n) {
                ctrl.nodes = n.data;               
            });
    }
]);

angular.module('SGA').controller('ForestryController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    '$stateParams',
    'loadingService',
    '$state',
    'reportService',
    function ($http, $resource, searchService, settings, stateParams, loadingService, $state, reportService) {

        var ctrl = this;
        ctrl.id = null;

        //Pegar os Contratos
        ctrl.prefix = reportService.prefix.query();


        //ctrl.changePrefix = function () {
        //    $http.get("http://localhost:10000/api/TerritorialUnit").then(
        //  function (n) {
        //      ctrl.nodes = n.data;
        //  });
        //}
        //this.busca = function () {
        //    cursor_wait();
        //    this.reportgroups = reportService.list.query({ module: $stateParams.module, idcontract: this.contrato }, function (result) {
        //        remover_cursor();
        //    })
        //};

        //Quando clicado em algum id, chama a tela de informações do id

        ctrl.my_tree_handler = function (branch) {
            $state.go('forestry.parcel.id', { id: branch.data.code });

            //var _ref;
            //ctrl.output = "You selected: " + branch.label;
            //if ((_ref = branch.data) !== null ? _ref.description : void 0) {
            //    return ctrl.output += '(' + branch.data.description + ')';
            //}
        }
        ctrl.apple_selected = function (branch) {
            return ctrl.output = "APPLE! : " + branch.label;
        };
        ctrl.my_tree = {};

        ctrl.nodes = [];
       
        //Pegas as informações de Arvore
        $http.get("http://localhost:10000/api/TerritorialUnit").then(
            function (n) {
                ctrl.nodes = n.data;               
            });
    }
]);

angular.module('SGA').controller('ForestryParcelController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    '$stateParams',
    'loadingService',
    function ($http, $resource, searchService, settings, stateParams, loadingService) {
        var ctrl = this;
        ctrl.Parcel = { Name: stateParams.id };

    }]);