﻿//proj4.defs("EPSG:4326", "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");// Já definido pelo proj4
proj4.defs("EPSG:4674", "+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs");
proj4.defs("EPSG:4618", "+proj=longlat +ellps=aust_SA +no_defs");

proj4.defs("EPSG:31978", "+proj=utm +zone=18 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31979", "+proj=utm +zone=19 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31980", "+proj=utm +zone=20 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31981", "+proj=utm +zone=21 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31982", "+proj=utm +zone=22 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31983", "+proj=utm +zone=23 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31984", "+proj=utm +zone=24 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:31985", "+proj=utm +zone=25 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");

proj4.defs("EPSG:32718", "+proj=utm +zone=18 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32719", "+proj=utm +zone=19 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32720", "+proj=utm +zone=20 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32721", "+proj=utm +zone=21 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32722", "+proj=utm +zone=22 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32723", "+proj=utm +zone=23 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32724", "+proj=utm +zone=24 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
proj4.defs("EPSG:32725", "+proj=utm +zone=25 +south +ellps=WGS84 +datum=WGS84 +units=m +no_defs");

proj4.defs("EPSG:29188", "+proj=utm +zone=18 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29189", "+proj=utm +zone=19 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29190", "+proj=utm +zone=20 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29191", "+proj=utm +zone=21 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29192", "+proj=utm +zone=22 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29193", "+proj=utm +zone=23 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29194", "+proj=utm +zone=24 +south +ellps=aust_SA +units=m +no_defs");
proj4.defs("EPSG:29195", "+proj=utm +zone=25 +south +ellps=aust_SA +units=m +no_defs");