﻿angular.module('SGA').directive('searchbar', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            sgaSearchService: '='
        },
        replace: true,
        template:
                '<div class="well">' +
                '<div class="input-group col-sm-6">' +
                '<div class="input-group-btn btn-group search-panel" uib-dropdown>' +
                '<button id="single-button" type="button" class="btn btn-primary" uib-dropdown-toggle>' +
                '{{ctrl.sgaSearchService.currentSearchType.description || "Filtrar por" }} <span class="caret"></span>' +
                '</button>' +
                '<ul class="uib-dropdown-menu" role="menu" aria-labelledby="single-button">' +
                '<li ng-repeat="t in ctrl.sgaSearchService.searchTypes" role="menuitem"><a ng-click="ctrl.setSearchType(t)">Por {{t.description}}</a></li>' +
                '</ul>' +
                '</div>' +
                '<input ng-readonly="!ctrl.sgaSearchService.currentSearchType" id="search_box" class="form-control" name="x" ng-model="ctrl.sgaSearchService.currentArgument" ng-change="ctrl.argumentChange()" placeholder="{{ctrl.sgaSearchService.currentSearchType ? \'Pesquisar...\' : \'Escolhe o tipo de pesquisa...\' }}">' +
                '</div>' +
                '</div>',
        controllerAs: 'ctrl',
        controller: function () {
            var ctrl = this;

            ctrl.argumentChange = function () {
                ctrl.sgaSearchService.updateList();
                console.log("ctrl.sgaSearchService.updateList();");
            };
            ctrl.setSearchType = function (t) {
                ctrl.sgaSearchService.setSearchType(t);
                console.log(" ctrl.sgaSearchService.setSearchType(t);");
            };

        }

    };
});