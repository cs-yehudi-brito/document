﻿angular.module('SGA').directive('loading', ['loadingService', function (loadingService) {
    return {
        restrict: 'E',
        controllerAs: 'ctrl',
        scope: {},
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            //contador
            ctrl.loadStatus = null;
            $scope.$watch(
                function () {
                    return loadingService.load;
                },
                function (newVal) { //Quando o valor muda esta função é executada
                    ctrl.loadStatus = !!newVal;
                    console.log(ctrl.loadStatus);
                })
        }],
        templateUrl: "assets/js/directives/templates/loading.html",
    }
}])