﻿angular.module('SGA').directive('sgaSelect', function () {
    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            items: '=sgaItems',
            change: '&sgaChange',
            selectedValue: '=sgaSelectedValue',
            textField: '@sgaTextField',
            valueField: '@sgaValueField',
            emptyMessage: '@sgaEmptyMessage'
        },
        replace: true,
        templateUrl: "assets/js/directives/templates/select.html",
        controllerAs: 'ctrl',
        controller: ['$scope', function ($scope) {
            var ctrl = this;
            ctrl.textField = ctrl.textField || "name";
            ctrl.valueField = ctrl.valueField || "id";
            ctrl.emptyMessage = ctrl.emptyMessage || "Itens não encontrados";
            
            $scope.$watchCollection(function () {
                return ctrl.items;
            }, function () {

                if (!ctrl.items) {
                    ctrl.selectedValue = null;
                }else if (ctrl.items.length === 1) {
                    ctrl.selectedValue = ctrl.items[0][ctrl.valueField];
                } else if (ctrl.items.length === 0) {
                    ctrl.selectedValue = null;
                };
            });

            $scope.$watch(
                   "ctrl.selectedValue",
                   function handleChange(newValue, oldValue) {
                       ctrl.change();
                   }
               );
          
        }
        
    ]};
});