﻿angular.module('SGA').directive('sgaInformation', function (searchService) {
    return {
        restric: 'E',
        scope: {},
        bindToController: {

            markerInf: '=sgaMarkerinf',

        },
        templateUrl: "assets/js/directives/templates/informationMap.html",
        controllerAs: 'ctrl',
        controller: ['$scope', function ($scope) {}],
    }
})