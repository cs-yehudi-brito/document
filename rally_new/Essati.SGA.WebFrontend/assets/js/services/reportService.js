﻿angular.module('SGA').factory('reportService', ['$resource', 'settings', reportService]);

function reportService($resource, settings) {

    return {

        //Função que retorna a lista de Contratos   
        prefix: $resource(settings.sgaApiBaseUrl + 'reports/prefixes', null, {
            'query': { method: 'GET', isArray: true } //is Array foi adicionado para ele aceitar o retorno de uma array de objeto
        }),
        //Função que retorna a lista com as Informações para preenchimento
        list: $resource(settings.sgaApiBaseUrl + 'reports?idcontract=:idcontract&module=:module', null,
            { 'query': { method: 'GET', params: { idcontract: '@idcontract',module:'@module' }, isArray: true } }
         ),
        //Envia para a API as informações para se fazer o download
        renderExcel: $resource(settings.sgaApiBaseUrl + 'reports/renderexcel', null,
            { 'save': { method: 'POST' } }
         ),
        renderPdf: $resource(settings.sgaApiBaseUrl + 'reports/renderpdf', null,
            { 'save': { method: 'POST'} }
         )
         
    }


}