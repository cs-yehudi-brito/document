﻿
angular
    .module('SGA')
    .factory('currentUser', ['$cookies' , currentUser]);

function currentUser($cookies) {
    var profile = {
        isLoggedIn: false,
        username: '',
        token: ''
    };

    var setProfile = function (username, token) {
        
        profile.username = username;
        profile.token = token;
        profile.isLoggedIn = true;
        $cookies.putObject('profile', profile);
    };

    var clearProfile = function () {

        profile.username = "";
        profile.token = "";
        profile.isLoggedIn = false;
        $cookies.remove("profile");
    };


    var getProfile = function () {
        if (!profile || (profile && !profile.isLoggedIn && $cookies.getObject('profile'))) {
           profile = $cookies.getObject('profile');
        }
        return profile;
    }

    return {
        setProfile: setProfile,
        getProfile: getProfile,
        clearProfile: clearProfile
    }
}

