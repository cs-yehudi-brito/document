﻿//Produção

//angular.module('SGA').value('settings',
//    {
//       // BaseURL of the SGA Web API URL
//        sgaApiBaseUrl: "http://api.sga.essati.com.br/api/",
//        sgaTokenUrl: "http://api.sga.essati.com.br/Token",
//        baseUrl: "http://sga.essati.com.br/api/",
//        //Requests for autocomplete are only send after the user input has stopped for X miliseconds.
//        autoCompleteDelay: 500
//    });
    

//Desenvolvimento

angular.module('SGA').value('settings',
    {
        //BaseURL of the SGA Web API URL
        sgaApiBaseUrl: "http://localhost:10000/api/",
        sgaTokenUrl: "http://localhost:10000/Token",
        baseUrl: "http://localhost:64479/api/",
        //Requests for autocomplete are only send after the user input has stopped for X miliseconds.
        autoCompleteDelay: 500
    });
