﻿angular.module('SGA').factory('userService', ['$q', '$timeout', function ($q, $timeout) {
    var user = undefined;
    return {
        getAuthObject: function () {
            var deferred = $q.defer();
            if (user) { return $q.when(user); }
            $timeout(function () {
                user = { isAuthenticated: false };
                deferred.resolve(user)
            }, 500)
            return deferred.promise;
        },
        isAuthenticated: function () {
            return user !== undefined && user.isAuthenticated;
        }
    };
    console.log(user.isAuthenticated);
}]);