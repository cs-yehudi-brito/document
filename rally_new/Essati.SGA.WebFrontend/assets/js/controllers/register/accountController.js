﻿angular.module('SGA').controller('AccountController', [
                    '$timeout'
                    , '$q'
                    ,'$state'
                    , '$http'
                    , '$resource'
                    , 'searchService'
                    , 'settings'
                    , '$scope'
                    , '$filter'
                    , '$stateParams'
                    , 'accountService'
                    , 'currentUser',
                    function (
                        $timeout,
                        $q,
                        $state,
                        $http,
                        $resource,
                        searchService,
                        settings,
                        $scope,
                        $filter,
                        $stateParams,
                        accountService,
                        currentUser) {
                        var ctrl = this;
                        this.listUser = [];
                        this.nomepapel = null;
                        this.checkemail = null;
                        this.checkpass = null;

                        //listar os Person
                        this.person = accountService.person.query();
                      
                        //Listar Role do Usuário Substituir userRoler para accountUser
                        this.user = accountService.accountUser.query({ email: $stateParams.email });
                                        
                        //Listar os  Papeis
                        this.listroles = accountService.listRole.query();
                        
                        this.listcontract = function (query) {
                            return  accountService.contract.query().$promise;
                        };

                        //Listar a Organização
                        this.organization = accountService.organizationList.query();

                        //Pegar o template do Modelo Usuário para Salvar
                        this.getuser = accountService.getAccountModel.query();

                        //this.save = function (value) { return accountService.saveUser.save(value); }
                        this.save = function () {
                            accountService.saveUser.save(this.getuser, function () { alert('Dados Salvos'); $state.go('account'); }, function () { alert('Problema em Salvar o Arquivo.'); });
                        
                        }
                            //accountService.save.put();
                        
                        //Deleta Usuario
                        this.delete = function (value) {
                            accountService.deleteAccount.delete({ id: value }, function () { alert('Usuário apagado com sucesso'); $state.go('account'); }, function () { alert('Problema em Apagar o Usuário.'); });
                        }

                        //Enviar os dados para serem salvos

                        this.addRole = function () {
                            ctrl.user.userRoles = ctrl.user.userRoles || [];
                            var i = ctrl.user.userRoles;
                            i.push({
                                idrole: "",
                                contractList: "",
                                state: 'Added',
                                isAdmin: "",
                            })

                        }
                        this.role = null;

                        this.changerole = function(role)
                        {
                            role.state = 'Modified';
                        }
                        this.deleterole = function(role)
                        {
                           role.state = 'Deleted';
                        }
                        this.saveAlter = function () {
                                                    
                            accountService.saveUser.save(this.user, function () { alert('Dados Salvos') }, function () { alert('Problema em Salvar o Arquivo.'); });
                        }
                        this.roledel = function () {
                            accountService.saveUser.save(ctrl.role, function () { alert('Dados Salvos') }, function () { alert('Problema em Salvar o Arquivo.'); });
                        }
                    }]);
