﻿angular.module('SGA').controller('ProgramController', [
        '$http', '$stateParams', 'searchService', 'Programs','HighwayWorkSites', function ($http, $stateParams, searchService, HighwayWorkSites) {

            this.search = searchService.Instance('highwayworksite');
            this.highwayWorkSite = HighwayWorkSites.get({ id: $stateParams.id });

        }]);



angular.module('SGA').factory('HighwayWorkSites', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highwayworksite/:id');

}]);


angular.module('SGA').factory('HighwaySections', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highwaysection/:id');

}]);