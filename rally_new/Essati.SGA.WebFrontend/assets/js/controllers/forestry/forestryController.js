﻿angular.module('SGA').controller('ForestryController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    '$stateParams',
    'loadingService',
    '$state',
    'reportService',
    function ($http, $resource, searchService, settings, stateParams, loadingService, $state, reportService) {

        var ctrl = this;
        ctrl.id = null;

        //Pegar os Contratos
        ctrl.prefix = reportService.prefix.query();


        //ctrl.changePrefix = function () {
        //    $http.get("http://localhost:10000/api/TerritorialUnit").then(
        //  function (n) {
        //      ctrl.nodes = n.data;
        //  });
        //}
        //this.busca = function () {
        //    cursor_wait();
        //    this.reportgroups = reportService.list.query({ module: $stateParams.module, idcontract: this.contrato }, function (result) {
        //        remover_cursor();
        //    })
        //};

        //Quando clicado em algum id, chama a tela de informações do id

        ctrl.my_tree_handler = function (branch) {
            $state.go('forestry.parcel.id', { id: branch.data.code });

            //var _ref;
            //ctrl.output = "You selected: " + branch.label;
            //if ((_ref = branch.data) !== null ? _ref.description : void 0) {
            //    return ctrl.output += '(' + branch.data.description + ')';
            //}
        }
        ctrl.apple_selected = function (branch) {
            return ctrl.output = "APPLE! : " + branch.label;
        };
        ctrl.my_tree = {};

        ctrl.nodes = [];
       
        //Pegas as informações de Arvore
        $http.get("http://localhost:10000/api/TerritorialUnit").then(
            function (n) {
                ctrl.nodes = n.data;               
            });
    }
]);
