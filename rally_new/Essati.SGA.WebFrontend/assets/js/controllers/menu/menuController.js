﻿angular.module('SGA').controller('MenuController', [
    '$http',
    '$resource',
    'searchService',
    'settings',
    'menuService',
    '$stateParams',
    function (
        $http,
        $resource,
        searchService,
        settings,
        menuService,
        $stateParams
        ) {
        var ctrl = this;

        ////Pegar todo o Menu
        this.index = menuService.menu.query();
        $(function() {
            $('[data-toggle="offcanvas"]').click(function(e) {
                e.preventDefault();
                var navside = '.nav-side-menu',
                $navsidetip = $('[data-toggle="tooltip-menu"]');
                $(navside + ', .content-wrapper').toggleClass('toggled');
                if ($(navside).hasClass('toggled')) {
                    $navsidetip.tooltip({
                        container: 'body'
                    });
                } else {
                    $navsidetip.tooltip('destroy');
                }
            });
        })
    }
]);