﻿angular.module('SGA').factory('Shape', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'gis/readshp/');

    }]);