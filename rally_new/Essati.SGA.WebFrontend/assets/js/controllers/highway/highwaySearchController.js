﻿angular.module('SGA').controller('HighwaySearchController', [
    '$http',
    'searchService',
    'Highway',
    'settings',
    'Upload',
    '$scope',
    '$stateParams',


    function ($http,
                        searchService,
                        Highway,
                        settings,
                        Upload,
                        $scope,
                        $stateParams

                        ) {

        var ctrl = this;
        ctrl.search = searchService.Instance('highway');
    }]);