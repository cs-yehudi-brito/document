﻿/*
$http - Get, Put, Post, Delete


*/
angular.module('SGA').controller('HighwayController', [
    '$http',
    'searchService',
    'settings',
    'Highway',
    'dialogService',
    'Upload',
    'messageService',
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    
    HighwayController]);


function HighwayController(
    $http,
    searchService,
    settings,
    Highway,
    dialogService,
    Upload,
    messageService,
    $rootScope,
    $scope,
    $state,
    $stateParams
    
          ) {

    var ctrl = this;

    //ctrl.add = add;
    ctrl.save = save;
    ctrl.delete = remove;
    ctrl.featureSelect = featureSelect;
    ctrl.convertSpatial = convertSpatial;
    ctrl.upload = upload;
   
    //this.search = searchService.Instance('highway');

    ctrl.highway = Highway.query({ id: $stateParams.id || 0 },
        function (h) {
            ctrl.map.paths = {};
            ctrl.map.paths[ctrl.highway.shape.type] = ctrl.convertSpatial(ctrl.highway.shape);
        }, function () {
           
        });

    // Create / Update
    function save() {
        Highway.save(
            this.highway,
           function () {
               var title = 'Salvo';
               var message = 'Arquivo salvo com sucesso';
               messageService.send(message, title);
           },
        function () {
            var title = 'Problema';
            var message = 'Problema ao salvar, contate o administrador';
            messageService.send(message, title);
        });

    }

    //Delete
    function remove() {       
        Highway.delete({ id: $stateParams.id },
        function () {
            var title =  'Apagado';
            var message = 'Arquivo apagado com sucesso';
            messageService.send(message, title);
        },
        function () {
            var title = 'Problema';
            var message = 'Problema ao apagar, contate o administrador';
            messageService.send(message, title);
        });
    }
    this.map = {
        center: {
            lat: -22.92045269,
            lng: -43.20854187,
            zoom: 5
        },
        layers: {
            baselayers: {
                mapbox_light: {
                    name: 'MapBox Light',
                    url: ''
                }
            }
        },
        paths: []
    }

    function featureSelect(feature) {
        ctrl.map.paths = {};
        ctrl.map.paths[feature.type] = ctrl.convertSpatial(feature);
        ctrl.highway.shape = feature;
    }

    function convertSpatial(feature) {
        console.log("Converting spatial feature:");
        var type = "polyline";
        var pathNew = {
            type: type,
            weight: 3,
            latlngs: new Array()
        };

        if (!!feature.lines) {
            pathNew.type = "multiPolyline";
            console.log("...MultiLine");
            for (var l = 0; l < feature.lines.length; l++) {
                pathNew.latlngs.push(new Array());
                var line = feature.lines[l];
                for (var i = 0; i < line.points.length; i++) {
                    pathNew.latlngs[l].push({
                        lat: line.points[i].lat,
                        lng: line.points[i].lng
                    });
                }
            }
        } else {
            console.log("...Line");
            for (var p = 0; p < feature.points.length; p++) {
                pathNew.latlngs.push({
                    lat: feature.points[p].lat,
                    lng: feature.points[p].lng
                });
            }
        }
        console.log("...Done");

        return pathNew;
    }
    //Pegando os dados do ID
   
    this.shapeFile = {};

    function upload(file) {
        Upload.upload({
            url: settings.sgaApiBaseUrl + "gis/readshp",
            data: { file: file }
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded.');//' Response: ' + resp.data);
            ctrl.shapeFile = resp.data;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });

        this.validation = {
            numero: 1
        }
    };

};


angular.module('SGA').factory('Highway', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highway/:id', null, {
        'put': { method: 'PUT' },
        'query':{method: 'GET'}
    });
}]);