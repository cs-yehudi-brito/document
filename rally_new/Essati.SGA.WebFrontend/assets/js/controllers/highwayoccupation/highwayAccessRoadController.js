﻿angular.module('SGA').controller('HighWayAccessRoadController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , 'leafletBoundsHelpers'
                 , 'leafletData'
                 , 'HighwayAccessRoads'
                 , 'Programs'
                 , 'dialogService'
                 , 'messageService'
                 , 'RoadSides'
                 , 'HighwayWorkSites'
                 , 'Directions'
                 , 'HighwaySections'
                 , 'Elements'
                 , 'Upload'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams'
                 , 'JudicialProcessStatus'
                 , 'NotificationType',

        function ($timeout, $q, searchService, settings, leafletBoundsHelpers, leafletData, HighwayAccessRoads, Programs, dialogService, messageService, RoadSides, HighwayWorkSites, Directions, HighwaySections, Elements, Upload, $scope, $filter, $state, $stateParams, JudicialProcessStatus, NotificationType) {

            var ctrl = this;

            this.showWeeks = false; //Remover barra lateral do datepicker

            this.accessTypes = [{ idaccessType: 1, name: "Comercial" }, { idaccessType: 2, name: "Industrial" }, { idaccessType: 3, name: "Residencial" }, { idaccessType: 4, name: "Lavoura" }, { idaccessType: 5, name: "Público" }, { idaccessType: 6, name: "Outros" }];

            this.pavementTypes = [{ idpavementType: 1, name: "Flexível" }, { idpavementType: 2, name: "Semi-Rígido" }, { idpavementType: 3, name: "Rígido" }, { idpavementType: 4, name: "Sem Revestimento" }];

            this.openCalendar = function (inspection) {
                //$event.preventDefault();
                //$event.stopPropagation();

                inspection.isOpen = true;
            };

            /* Map vars and functions*/
            this.mapMarkers = new Array();
            this.updateMap = function () {
                var bounds = L.latLngBounds(ctrl.mapMarkers);
                if (bounds.isValid()) {
                    ctrl.center = bounds.getCenter();
                    leafletData.getMap().then(function (map) {
                        map.fitBounds(bounds.pad(0.5));
                        if (ctrl.mapMarkers.length === 1) {
                            map.setZoom(12);
                        }
                    });
                }
            }

            ctrl.selectedHighwaySection = null;

            $scope.$watch('ctrl.accessRoad.idhighwaySection', function () {
                ctrl.selectedHighwaySection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.accessRoad.idhighwaySection }, true)[0];
                ctrl.updateDirectionCollection();

            });

            $scope.$watchCollection(function () {
                return ctrl.highwaySections;
            }, function () {
                ctrl.updateDirectionCollection();
            });


            //Collections that are used to fill in the UI elements
            this.contracts = [{ "name": "EE-53", "idcontract": 6 }];

            this.cities = [
                { idterritorialunit: 9895, name: "Areal/RJ" },
                { idterritorialunit: 9897, name: "Comendador Levy Gasparian/RJ" },
                { idterritorialunit: 9911, name: "Três Rios/RJ" },
                { idterritorialunit: 10121, name: "Petrópolis/RJ" },
                { idterritorialunit: 10163, name: "Duque de Caxias/RJ" },
                { idterritorialunit: 10231, name: "Rio de Janeiro/RJ" },
                { idterritorialunit: 9242, name: "Juiz de Fora/MG" },
                { idterritorialunit: 9274, name: "Matias Barbosa/MG" },
                { idterritorialunit: 9323, name: "Simão Pereira/MG" }

            ];

            this.highwaySections = [];
            this.directions = [];
            this.roadsides = RoadSides.query(); //get all roadsides (generic to all contracts, no need to filter here)
            this.judicialProcessStatusLookup = JudicialProcessStatus.query();
            this.notificationTypeLookup = NotificationType.query();

            //Updates the direction colection and also sets the selected section
            //TODO: Rename this function to reflect its actual function
            this.updateDirectionCollection = function () {
                var arr = [];

                if (ctrl.highwaySections === null || ctrl.highwaySections.length === 0) {

                } else {
                    if (ctrl.accessRoad.idhighwaySection === null) {
                        angular.forEach(ctrl.highwaySections, function (key, value) {
                            $.extend(true, arr, key.directions);
                        });
                        ctrl.directions = arr;
                    } else {
                        var selectedSection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.accessRoad.idhighwaySection }, true);
                        ctrl.directions = selectedSection[0].directions;
                    }
                }
            }

            //Function that gets all necessary location information based on the filter (km, highway and contract)
            this.updateLocationFields = function () {
                console.log("Update location fields");
                var self = this;

                //Passar o Code para o breadcump  
                this.code = ctrl.accessRoad.code;

                //set all variables to zero if no position was chosen
                if (!ctrl.accessRoad.positionStart) {
                    ctrl.directions = [];
                    ctrl.highwaySections = [];
                    ctrl.accessRoad.iddirection = null;
                    ctrl.selectedHighwaySection = null;
                    ctrl.accessRoad.idhighwaySection = null;
                    return;
                }

                //Define the lookup parameters
                var par = {
                    idhighway: ctrl.idhighway,
                    kmfilter: ctrl.accessRoad.positionStart,
                    icontract: ctrl.accessRoad.idcontract
                }

                if (this.wsSearchPending) {
                    $timeout.cancel(this.wsSearchPending);
                }

                //Function that updates the list of Worksites (depends on accessRoad.km)
                //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
                this.wsSearchPending = $timeout(
                    function () {
                        console.log("Getting sections");
                        self.highwaySections = HighwaySections.query(par, function () {
                        });

                    }, settings.autoCompleteDelay); //Configurable delay used here
            };

            this.accessRoad = HighwayAccessRoads.get({ id: $stateParams.id || 0 },
                function (o) {
                    console.log("HighwayAccessRoad loaded");
                    ctrl.updateLocationFields();
                    ctrl.accessRoad.idcontract = ctrl.contracts[0].idcontract;

                    //transform coordinate in to array. Required by Leaflet
                    ctrl.accessRoad.position = ctrl.accessRoad.position ? ctrl.accessRoad.position : { points: [] };
                    var coord = (ctrl.accessRoad.position && ctrl.accessRoad.position.points) ? ctrl.accessRoad.position.points : [];
                    coord.forEach(function (e, i, a) {
                        ctrl.mapMarkers.push({ lat: e.lat, lng: e.lng });
                    });

                    //transform the dates of the actions (needed for validation)
                    ctrl.accessRoad.actions.forEach(function (e, i, a) {
                        e.performedOn = new Date(e.performedOn);
                    });


                    ctrl.updateMap();

                });
            
            this.save = function() {
                ctrl.accessRoad.$save(
                  function (x, y) {
                      var title = 'Salvo';
                      var message = 'Arquivo salvo com sucesso';
                      messageService.send(message, title);
                      $state.go('highwayaccessroads.id', { id: ctrl.accessRoad.idaccessRoad }, { reload: true });
                  },
                    function () {
                        var title = 'Problema';
                        var message = 'Problema ao salvar, contate o administrador';
                        messageService.send(message, title);
                    });
            }


            this.delete = function () {
               HighwayAccessRoads.delete({ id: $stateParams.id },
              function () {
                  var title = 'Apagado';
                  var message = 'Arquivo apagado com sucesso';
                  messageService.send(message, title);
                  $state.go('highwayaccessroads');
              },
                function () {
                    var title = 'Problema';
                    var message = 'Problema ao apagar, contate o administrador';
                    messageService.send(message, title);
                });
            }

            this.cancel = function() {
                $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
            }

            this.addInspection = function () {
                ctrl.accessRoad.actions = ctrl.accessRoad.actions || [];
                var i = ctrl.accessRoad.actions;
                i.push({
                    performedOn: new Date(moment().format()),
                    description: "",
                    state: 'Added'
                });

            }

            this.deleteInspection = function (action) {
                action.state = 'Deleted';
            }

            this.markInspectionAsChanged = function (action) {
                if (action.state !== 'Added') {
                    action.state = 'Modified';
                }
            }

            this.checkValids = function () {
                console.log($scope.validation.$error.required);
            }

        }]);


angular.module('SGA').factory('HighwayAccessRoads', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'highwayaccessroad/:id');

}]);




