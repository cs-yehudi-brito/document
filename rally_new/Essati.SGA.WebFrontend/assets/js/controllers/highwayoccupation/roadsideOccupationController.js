﻿angular.module('SGA').controller('RoadsideOccupationController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , 'leafletBoundsHelpers'
                 , 'leafletData'
                 , 'RoadsideOccupations'
                 , 'Programs'
                 , 'dialogService'
                 , 'messageService'
                 , 'RoadSides'
                 , 'HighwayWorkSites'
                 , 'Directions'
                 , 'HighwaySections'
                 , 'Elements'
                 , 'Upload'
                 , '$scope'
                 , '$filter'
                 , '$state'
                 , '$stateParams'
                 , 'JudicialProcessStatus'
                 , 'NotificationType'
                 ,

        function ($timeout, $q, searchService, settings, leafletBoundsHelpers, leafletData, RoadsideOccupations, Programs, dialogService, messageService, RoadSides, HighwayWorkSites, Directions, HighwaySections, Elements, Upload, $scope, $filter, $state, $stateParams, JudicialProcessStatus, NotificationType) {

            var ctrl = this;

            this.showWeeks = false; //Remover barra lateral do datepicker

            this.id = $stateParams.id;

            //this.search = searchService.Instance('roadsideoccupation');

            this.judicialProcessDistributionDateIsOpen = {};

            this.landUses = [{ idlandUse: 1, name: "Comercial" }, { idlandUse: 2, name: "Industrial" }, { idlandUse: 3, name: "Residencial" }, { idlandUse: 4, name: "Lavoura" }, { idlandUse: 5, name: "Público" }, { idlandUse: 6, name: "Passivo Ambiental" }, { idlandUse: 7, name: "Outros" }];
            //this.pavementTypes = [{ idpavementType: 1, name: "Flexível" }, { idpavementType: 2, name: "Semi-Rígido" }, { idpavementType: 3, name: "Rígido" }, { idpavementType: 4, name: "Sem Revestimento" }];
            this.domainSituations = [{ iddomainSituation: 1, name: "Faixa de Domínio" }, { iddomainSituation: 2, name: "Faixa Não Edificante" }, { iddomainSituation: 3, name: "Faixa de Domínio e Não Edificante" }];
            this.roadObjects = [{ idroadsideObject: 1, name: "Telecomunicação" }, { idroadsideObject: 2, name: "Ponto de Ônibus" }, { idroadsideObject: 3, name: "Elétrica" }, { idroadsideObject: 4, name: "Painel" }, { idroadsideObject: 5, name: "Edificação" }, { idroadsideObject: 6, name: "Outros" }];
            this.openCalendar = function (cal) {
                cal.isOpen = true;
            };

            /* Map vars and functions*/
            this.mapMarkers = new Array();
            this.updateMap = function () {
                var bounds = L.latLngBounds(ctrl.mapMarkers);
                if (bounds.isValid()) {
                    ctrl.center = bounds.getCenter();
                    leafletData.getMap().then(function (map) {
                        map.fitBounds(bounds.pad(0.5));
                        if (ctrl.mapMarkers.length === 1) {
                            map.setZoom(12);
                        }
                    });
                }
            }

            ctrl.selectedHighwaySection = null;

            $scope.$watch('ctrl.roadsideOccupation.idhighwaySection', function () {
                ctrl.selectedHighwaySection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.roadsideOccupation.idhighwaySection }, true)[0];
                ctrl.updateDirectionCollection();

            });

            $scope.$watchCollection(function () {
                return ctrl.highwaySections;
            }, function () {
                ctrl.updateDirectionCollection();
            });


            //Collections that are used to fill in the UI elements
            this.contracts = [{ "name": "EE-53", "idcontract": 6 }];

            this.cities = [
                { idterritorialunit: 9895, name: "Areal/RJ" },
                { idterritorialunit: 9897, name: "Comendador Levy Gasparian/RJ" },
                { idterritorialunit: 9911, name: "Três Rios/RJ" },
                { idterritorialunit: 10121, name: "Petrópolis/RJ" },
                { idterritorialunit: 10163, name: "Duque de Caxias/RJ" },
                { idterritorialunit: 10231, name: "Rio de Janeiro/RJ" },
                { idterritorialunit: 9242, name: "Juiz de Fora/MG" },
                { idterritorialunit: 9274, name: "Matias Barbosa/MG" },
                { idterritorialunit: 9323, name: "Simão Pereira/MG" }

            ];
           
            this.highwaySections = [];
            this.directions = [];
            this.roadsides = RoadSides.query(); //get all roadsides (generic to all contracts, no need to filter here)

            this.judicialProcessStatusLookup = JudicialProcessStatus.query();

            this.notificationTypeLookup = NotificationType.query();

            //Updates the direction colection and also sets the selected section
            //TODO: Rename this function to reflect its actual function
            this.updateDirectionCollection = function () {
                var arr = [];

                if (ctrl.highwaySections === null || ctrl.highwaySections.length === 0) {

                } else {
                    if (ctrl.roadsideOccupation.idhighwaySection === null) {
                        angular.forEach(ctrl.highwaySections, function (key, value) {
                            $.extend(true, arr, key.directions);
                        });
                        ctrl.directions = arr;
                    } else {
                        var selectedSection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.roadsideOccupation.idhighwaySection }, true);
                        ctrl.directions = selectedSection[0].directions;
                    }
                }
            }

            //Function that gets all necessary location information based on the filter (km, highway and contract)
            this.updateLocationFields = function () {
                console.log("Update location fields");
                var self = this;

                //Passar o Code para o breadcump  
                this.code = ctrl.roadsideOccupation.code;                

                //set all variables to zero if no position was chosen
                if (!ctrl.roadsideOccupation.positionStart) {
                    ctrl.directions = [];
                    ctrl.highwaySections = [];
                    ctrl.roadsideOccupation.iddirection = null;
                    ctrl.selectedHighwaySection = null;
                    ctrl.roadsideOccupation.idhighwaySection = null;
                    return;
                }

                //Define the lookup parameters
                var par = {
                    idhighway: ctrl.idhighway,
                    kmfilter: ctrl.roadsideOccupation.positionStart,
                    icontract: ctrl.roadsideOccupation.idcontract
                }

                if (this.wsSearchPending) {
                    $timeout.cancel(this.wsSearchPending);
                }

                //Function that updates the list of Worksites (depends on roadsideOccupation.km)
                //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
                this.wsSearchPending = $timeout(
                    function () {
                        console.log("Getting sections");
                        self.highwaySections = HighwaySections.query(par, function () {
                        });

                    }, settings.autoCompleteDelay); //Configurable delay used here
            };

            this.roadsideOccupation = RoadsideOccupations.get({ id: $stateParams.id || 0 },

                function (o) {
                    console.log("HighwayAccessRoad loaded");

                    ctrl.updateLocationFields();
                    ctrl.roadsideOccupation.idcontract = ctrl.contracts[0].idcontract;
                    
                    //transform coordinate in to array. Required by Leaflet
                    ctrl.roadsideOccupation.position = ctrl.roadsideOccupation.position ? ctrl.roadsideOccupation.position : { points: [] };
                    var coord = (ctrl.roadsideOccupation.position && ctrl.roadsideOccupation.position.points) ? ctrl.roadsideOccupation.position.points : [];
                    coord.forEach(function (e, i, a) {
                        ctrl.mapMarkers.push({ lat: e.lat, lng: e.lng });
                    });

                    //transform the dates of the actions (needed for validation)
                    ctrl.roadsideOccupation.actions.forEach(function (e, i, a) {
                        e.performedOn = new Date(e.performedOn);
                    });

                    //transform the other dates on the form
                    ctrl.roadsideOccupation
                        .judicialProcessDistributionDate = new
                        Date(ctrl.roadsideOccupation.judicialProcessDistributionDate);
                    
                    ctrl.updateMap();
                   

                });
       
            
            this.save = function () {
                ctrl.roadsideOccupation.$save(
                    function (x, y) {
                        var title = 'Salvo';
                        var message = 'Item salvo com sucesso';
                        messageService.send(message, title);
                        $state.go('roadsideoccupations.id', { id: ctrl.roadsideOccupation.idroadsideOccupation }, { reload: true });
                    },
                    function () {
                        var title = 'Problema';
                        var message = 'Problema ao salvar, contate o administrador';
                        messageService.send(message, title);
                    });
            }


            this.delete = function () {
               RoadsideOccupations.delete({ id: $stateParams.id },
               function () {
                   var title = 'Apagado';
                   var message = 'Item apagado com sucesso';
                   messageService.send(message, title);
                   $state.go('roadsideoccupations');
               },
                function () {
                    var title = 'Problema';
                    var message = 'Problema ao apagar, contate o administrador';
                    messageService.send(message, title);
                });
            }

            this.cancel = function() {
                $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
            }

            this.addInspection = function () {
                ctrl.roadsideOccupation.actions = ctrl.roadsideOccupation.actions || [];
                var i = ctrl.roadsideOccupation.actions;
                i.push({
                    performedOn: new Date(moment().format()),
                    description: "",
                    state: 'Added'
                });

            }

            this.deleteInspection = function (action) {
                action.state = 'Deleted';
            }

            this.markInspectionAsChanged = function (action) {
                if (action.state !== 'Added') {
                    action.state = 'Modified';
                }
            }

            this.checkValids = function () {
                console.log($scope.validation.$error.required);
            }

        }]);


angular.module('SGA').factory('RoadsideOccupations', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'roadsideoccupation/:id');

    
}]);

angular.module('SGA').factory('JudicialProcessStatus', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'judicialprocessstatus/:id');
    
}]);

angular.module('SGA').factory('NotificationType', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'notificationtype/:id');

}]);

//angular.module('SGA').controller('IncidentInspection', ['$modalInstance', 'inspection', function ($modalInstance, inspection) {
//    this.inspection = inspection;

//    this.ok = function () {
//        $modalInstance.close('ok');
//    };

//    this.cancel = function () {
//        $modalInstance.dismiss('cancel');
//    };
//}]);


