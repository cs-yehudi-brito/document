﻿angular.module('SGA').controller('AppAccessRoadController', [
    '$http',
    '$sce',
    'searchService',
    'settings',
    '$scope',
    '$stateParams',
    'appService',
    function ($http,
    $sce,
    searchService,
    settings,
    $scope,
    $stateParams,
    appService) {
        var ctrl = this;
        ctrl.markers = [];
        ctrl.state = "app.accessroadid";
        ctrl.accessRoad = appService.get.query(null, function () {
            var i = 0;
            while (i < ctrl.accessRoad.length) {
                if (ctrl.markers.length == ctrl.accessRoad.length) {
                    break;
                }
                //if (ctrl.accessRoad[i].position.length == 0) {
                //    console.log("Position Vazio");
                //    break
                //}
                //else if (ctrl.accessRoad[i].postion.points[0] == null) {
                //    console.log("Points null");
                //    break
                //}
                //else {
                    ctrl.markers.push({ //tratar caso seja nulo
                        id: ctrl.accessRoad[i].idaccessRoad,
                        lat: ctrl.accessRoad[i].position.points[0].lat,
                        lng: ctrl.accessRoad[i].position.points[0].lng,
                    })
                //}
                i++;
            }
        });

        ctrl.temp = [];
        ctrl.idaccessroad = appService.getAccessRoadID.query({ id: $stateParams.id || 0 });
        console.log(ctrl.idaccessroad);
        $http.get(settings.sgaApiBaseUrl + 'app/accessroad/' + $stateParams.id).then(
                        function (data) {
                            ctrl.temp.push(data.data);
                            
                            return data.data;

                        }
            );
        
        

    }]);