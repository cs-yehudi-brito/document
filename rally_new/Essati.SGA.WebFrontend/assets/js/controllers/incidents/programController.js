﻿angular.module('SGA').controller('ProgramController', [
        '$http', '$stateParams', 'searchService', 'Programs', function ($http, $stateParams, searchService, Programs) {

            this.search = searchService.Instance('program');
            this.program = Programs.get({ id: $stateParams.id });

        }]);



angular.module('SGA').factory('Programs', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'program/:id');

}]);