﻿angular.module('SGA').controller('IncidentController', [
                 '$timeout'
                 , '$q'
                 , 'searchService'
                 , 'settings'
                 , 'leafletBoundsHelpers'
                 , 'leafletData'
                 , 'Incidents'
                 , 'dialogService'
                 , 'messageService'
                 , 'Programs'
                 , 'RoadSides'
                 , 'HighwayWorkSites'
                 , 'Directions'
                 , 'HighwaySections'
                 , 'Elements'
                 , 'Upload'
                 , '$scope'
                 , '$filter'
                 , '$stateParams',
        function ($timeout, $q, searchService, settings, leafletBoundsHelpers, leafletData, Incidents, dialogService, messageService, Programs, RoadSides, HighwayWorkSites, Directions, HighwaySections, Elements, Upload, $scope, $filter, $stateParams) {

            var ctrl = this;

            //this.search = searchService.Instance('incident');


            /* Map vars and functions*/
            this.mapMarkers = new Array();
            this.updateMap = function () {
                var bounds = L.latLngBounds(ctrl.mapMarkers);
                if (bounds.isValid()) {
                    ctrl.center = bounds.getCenter();
                    leafletData.getMap().then(function (map) {
                        map.fitBounds(bounds.pad(0.5));
                    });
                }
            }

            ctrl.selectedHighwaySection = null;

            $scope.$watch('ctrl.incident.idhighwaySection', function () {
                ctrl.selectedHighwaySection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.incident.idhighwaySection }, true)[0];
                ctrl.updateDirectionCollection();

            });

            $scope.$watchCollection(function () {
                return ctrl.highwaySections;
            }, function () {
                ctrl.updateDirectionCollection();
            });


            //Collections that are used to fill in the UI elements
            this.contracts = [{ "name": "EE-53", "idcontract": 6 }];

            this.highwaySections = [];
            this.worksites = [];
            this.directions = [];
            this.programs = Programs.query(); //get all programs //TODO: Filter by contract
            this.roadsides = RoadSides.query(); //get all roadsides (generic to all contracts, no need to filter here)
            this.elements = Elements.query(); // Get all elements. At the moment, these are the same for all projects, no need to filter
            this.loadElements = function (query) { // the tags directive uses a promise for autocomplete. This function provides that
                var deferred = $q.defer();
                deferred.resolve(ctrl.elements);
                return deferred.promise;
            };

            //Updates the direction colection and also sets the selected section
            //TODO: Rename this function to reflect its actual function
            this.updateDirectionCollection = function () {
                var arr = [];

                if (ctrl.highwaySections === null || ctrl.highwaySections.length === 0) {

                } else {
                    if (ctrl.incident.idhighwaySection === null) {
                        angular.forEach(ctrl.highwaySections, function (key, value) {
                            $.extend(true, arr, key.directions);
                        });
                        ctrl.directions = arr;
                    } else {
                        var selectedSection = $filter('filter')(ctrl.highwaySections, { idhighwaySection: ctrl.incident.idhighwaySection }, true);
                        ctrl.directions = selectedSection[0].directions;
                    }
                }
            }

            //Function that gets all necessary location information based on the filter (km, highway and contract)
            this.updateLocationFields = function () {
                console.log("Update location fields");
                var self = this;

                //Passar o Code para o breadcrumb
                this.code = ctrl.incident.code;

                //set all variables to zero if no position was chosen
                if (!ctrl.incident.positionStart) {
                    ctrl.worksites = []; //away
                    ctrl.idhighwayWorkSite = null; //away
                    ctrl.directions = [];
                    ctrl.highwaySections = [];
                    ctrl.incident.iddirection = null;
                    ctrl.incident.idhighwayWorkSite = null;
                    ctrl.selectedHighwaySection = null;
                    ctrl.incident.idhighwaySection = null;
                    return;
                }

                //Define the lookup parameters
                var par = {
                    idhighway: ctrl.idhighway,
                    kmfilter: ctrl.incident.positionStart,
                    icontract: ctrl.incident.idcontract
                }

                if (this.wsSearchPending) {
                    $timeout.cancel(this.wsSearchPending);
                }

                //Function that updates the list of Worksites (depends on incident.km)
                //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
                this.wsSearchPending = $timeout(
                    function () {
                        console.log("Getting sections");
                        self.highwaySections = HighwaySections.query(par, function () {
                        });

                    }, settings.autoCompleteDelay); //Configurable delay used here
            };

            //this.updateWorkSites = function () {
            //    console.log("update ws");

            //    //In case the starting position is null, clear the worksite collection and the current worksite. Don't do anything after that 
            //    if (ctrl.incident.positionStart == "") {
            //        ctrl.worksites = [];
            //        ctrl.idhighwayWorkSite = null;
            //        ctrl.directions = [];
            //        ctrl.incident.iddirection = null;
            //        return;
            //    }

            //    var par = { kmfilter: ctrl.incident.positionStart };

            //    if (this.wsSearchPending) {
            //        $timeout.cancel(this.wsSearchPending);
            //    }

            //    //Function that updates the list of Worksites (depends on incident.km)
            //    //To avoid hammering the webservice, we only really make the request after updateList hasn't been called for the time set in "autoCompleteDelay"
            //    this.wsSearchPending = $timeout(
            //        function () {
            //            console.log("getting ws");
            //            self.worksites = HighwayWorkSites.query(par, function () {
            //                if (self.worksites.length === 1) {
            //                    self.incident.idhighwayWorkSite = self.worksites[0].idhighwayWorkSite;
            //                    ctrl.directions = Directions.query({ idhighwayWorkSiteFilter: ctrl.incident.idhighwayWorkSite }, function (d) { console.log("Directions Loaded"); });
            //                } else if (self.worksites.length === 0) {
            //                    self.incident.idhighwayWorkSite = null;
            //                    ctrl.directions = [];
            //                    ctrl.incident.iddirection = null;
            //                }
            //            });

            //        }, settings.autoCompleteDelay); //Configurable delay used here
            //};

            //AFTER the incident is returned from the server, load the worksites and after that, the directions as well
            this.incident = Incidents.get({ id: $stateParams.id || 0 },

                function (o) {
                    console.log("Incident loaded");
                    ctrl.updateLocationFields();
                    ctrl.incident.idcontract = ctrl.contracts[0].idcontract;
                    //ctrl.worksites = ctrl.incident && ctrl.incident.state == 'New' ? [] : HighwayWorkSites.query({ km: ctrl.incident.start }, function (h) {
                    //    console.log("Worksites loaded");
                    //    ctrl.directions = Directions.query({ idhighwayWorkSiteFilter: ctrl.incident.idhighwayWorkSite }, function (d) { console.log("Directions Loaded"); });
                    //});

                    //transform coordinate in to array. Required by Leaflet
                    var coord = (ctrl.incident.coordinate && ctrl.incident.coordinate.points) ? ctrl.incident.coordinate.points : [];
                    coord.forEach(function (e, i, a) {
                        ctrl.mapMarkers.push({ lat: e.lat, lng: e.lng });
                    });

                    ctrl.updateMap();

                });

            this.save = function () {
                //ctrl.incident.$save();// Incidents.save(this.incident, null, function () { alert('Ocorrência gravada com sucesso') }, function () { alert('Problema ao gravar a ocorrência') });
                ctrl.incident.$save(
                    function (x, y) {
                        var title = 'Salvo';
                        var message = 'Arquivo salvo com sucesso';
                        messageService.send(message, title);
                    },
                    function () {
                        var title = 'Problema';
                        var message = 'Problema ao salvar, contate o administrador';
                        messageService.send(message, title);
                    });

                //, function(x,y) {alert('Problema em gravar os dados');
            }


            this.delete = function () {
                Incidents.delete({ id: $stateParams.id },
               function () {
                   var title = 'Apagado';
                   var message = 'Arquivo apagado com sucesso';
                   messageService.send(message, title);
               },
                function () {
                    var title = 'Problema';
                    var message = 'Problema ao apagar, contate o administrador';
                    messageService.send(message, title);
                });
            }


            this.addInspection = function () {
                var i = ctrl.incident.inspections;
                if (i.length === 0) {
                    i.push({
                        number: 1,
                        date: moment().format(),
                        comment: "-",
                        state: 'Added',
                        photos: [],
                        idstatus: 2
                    });
                } else {
                    i.push({
                        number: i[i.length - 1].number + 1,
                        date: moment().format(),
                        comment: "-",
                        state: 'Added',
                        photos: [],
                        idstatus: 1
                    });
                }
            }

            this.deleteInspection = function (inspection) {
                inspection.state = 'Deleted';
            }

            this.markInspectionAsChanged = function (inspection) {
                if (inspection.state !== 'Added') {
                    inspection.state = 'Modified';
                }
            }

        }]);


angular.module('SGA').factory('Incidents', ['$resource', 'settings', function ($resource, settings) {

    return $resource(settings.sgaApiBaseUrl + 'incident/:id', null, {
        'delete': { method: 'DELETE' },
        'contracts': { method: 'GET', isArray: true, url: settings.sgaApiBaseUrl + 'incidents/contracts' },
    });


    //TODO definir os metodos de gerenciamento de: elementos; vistorias e suas fotos

}]);

//angular.module('SGA').controller('IncidentInspection', ['$modalInstance', 'inspection', function ($modalInstance, inspection) {
//    this.inspection = inspection;

//    this.ok = function () {
//        $modalInstance.close('ok');
//    };

//    this.cancel = function () {
//        $modalInstance.dismiss('cancel');
//    };
//}]);


