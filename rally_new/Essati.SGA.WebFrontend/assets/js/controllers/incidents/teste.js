﻿angular.module('SGA').controller('TestController', function () {
    this.dynamicPopover = {
        content: 'Hello, World!',
        templateUrl: 'myPopoverTemplate.html',
        title: 'Title'
    };
});