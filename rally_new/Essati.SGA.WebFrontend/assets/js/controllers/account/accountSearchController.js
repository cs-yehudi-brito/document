﻿angular.module('SGA').controller('AccountSearchController', [
                    '$timeout'
                    , '$q'
                    , '$state'
                    , '$http'
                    , '$resource'
                    , 'searchService'
                    , 'settings'
                    , '$scope'
                    , '$filter'
                    , '$stateParams'
                    , 'accountService'
                    , 'currentUser',
                    function (
                        $timeout,
                        $q,
                        $state,
                        $http,
                        $resource,
                        searchService,
                        settings,
                        $scope,
                        $filter,
                        $stateParams,
                        accountService,
                        currentUser) {
                        var ctrl = this;
                      

                        //listar os Person
                        this.person = accountService.person.query();
                        ctrl.search = searchService.Instance('account');
                        //console.log(ctrl.search);
                        ctrl.parametros = accountService.params.query();
                    }]);