﻿angular.module('SGA').controller('ReportController', [
    '$http',
    '$resource',
    'searchService',
    'settings', 
    'reportService',
    '$stateParams',

    function (
        $http,
        $resource,
        searchService,
        settings,
        reportService,
        $stateParams
                     ) {
        var ctrl = this;

        this.currentreport = null;
        this.contrato = null;

        //Lista todos os contratos
        this.prefix = reportService.prefix.query();

        //Foi criado esta função para que quando ela fosse chamada, a mesma retornaria todos os relatórios atrelados ao contrato
        this.busca = function () {
            cursor_wait();
            this.reportgroups = reportService.list.query({module:$stateParams.module, idcontract: this.contrato }, function (result) {
                remover_cursor();
            })
        };
        //Função que verifica se existe algum dado dentro do objeto parametro 
        this.obj = function (data) {
            for (var i in data) if (data.hasOwnProperty(i)) return false;
            return true;
        }
        //Função do download no formato Excel
        this.excel = function (list) {
            this.reportlist = list;
            cursor_wait();
            //console.log(this.reportlist);
            //Envia as informações do relatorio para o servidor e espera a chave para fazer o download 
            var uid = reportService.renderExcel.save(this.reportlist,
                function () {
                    //Solicita ao servidor o download da chave passado pelo UID
                    window.location = settings.sgaApiBaseUrl + 'reports/download?fileName=' + uid.fileName + '&key=' + uid.key;
                    remover_cursor();
                });
        }
        //Função do download no formato PDF
        this.pdf = function (list) {
            this.reportlist = list;
            cursor_wait();
            //console.log(this.reportlist);
            var uid = reportService.renderPdf.save(this.reportlist,
                function () {
                    window.location = settings.sgaApiBaseUrl + 'reports/download?fileName=' + uid.fileName + '&key=' + uid.key;
                    remover_cursor();
                });
        }
        //Função para mudar o simbolo do  cursor
        function cursor_wait() {
            // switch to cursor wait for the current element over
            var elements = $(':hover');
            if (elements.length) {
                // get the last element which is the one on top
                elements.last().addClass('cursor-wait');
            }
            // add class to use it in the mouseover selector (to avoid conflicts)
            $('html, body').addClass('cursor-wait');
            // switch to cursor wait for all elements you'll be over
            $('html').on('mouseover', 'body.cursor-wait*', function (e) {
                $(e.target).addClass('cursor-wait');
            });
        }
        function remover_cursor() {
            $('html').off('mouseover', 'body.cursor-wait *'); // remove event handler
            $('.cursor-wait').removeClass('cursor-wait'); // get back to default
        }


    }
]);
