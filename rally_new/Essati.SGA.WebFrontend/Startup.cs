﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Essati.SGA.WebFrontend.Startup))]
namespace Essati.SGA.WebFrontend
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
