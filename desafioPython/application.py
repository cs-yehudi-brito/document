# -*- coding: utf-8 -*- 
from flask import Flask
from flask import request
from flask import jsonify
import json
import jwt

#Importando os Models
from Models.userInsert import insert
from Models.userGet import getUser, getAllUser
from Models.deleteUser import delete
from Models.updateUser import update
app = Flask(__name__)

@app.route('/getjson', methods = ['GET'])
def getAll():
    data = getAllUser()
    return jsonify(data)

@app.route('/getuser/<id>', methods = ['GET'])
def get(id):
    data = getUser(id)
    return jsonify(data)

@app.route('/putjson', methods = ['PUT'])
def put():
    #print(request.is_json) #verificar se os dados estão vindo em JSON
    content = request.get_json()
    status = insert(content)
    #print(status)
    return jsonify(status)

@app.route('/postuser/<id>', methods=['POST'])
def post(id):
    print(request.is_json)
    content = request.get_json()
    status = update(id, content)
    return jsonify(status)


@app.route('/delete/<id>', methods = ['DELETE'])
def deleteU(id):
    status = delete(id)
    return jsonify(status)

app.run(host='0.0.0.0', port= 8090)