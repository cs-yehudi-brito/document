#-*- coding: utf-8 -*-

import sqlite3 #importando o modulo para conectar no banco
import json
import sys
#Conectando no Banco


def insert(data):
    conn = sqlite3.connect('usuario.db') #Instancia o Banco
    cursor = conn.cursor() #Conectar no Banco
    #print(len(data))
    #file = json.dumps(data)
    phone = ''
    createTable()   
    for key, value in data.items():
        if key == "name":
            p_name = value
        elif key == "email":
            p_email = value
        elif key == "password":
            p_password = value
        elif key == "phones":
            for v in value:
                phone = v['ddd']+' '+v['number']
            p_phone = phone
    #print(p_name + ' ' + p_email + ' '+ p_password + ' ' + p_phone)
    status_email = checkemail(p_email)
    if status_email == False:
        cursor.execute("""
            INSERT INTO usuario(name, email, password, phone)VALUES(?, ?, ?, ? )
                """, (p_name, p_email, p_password, p_phone))
        try:
            conn.commit()
            texto = 'Dados gravos com sucesso'
        except:
            print(sys.exc_info())
            texto = 'Problemas para Cadastrar'
    else:
        texto = 'O Email já existe, logo não será possível o cadastro'
        print(texto)
    msg = { 'messagem': texto}
    conn.close()    
    return msg



def checkemail(mail):
    conn = sqlite3.connect('usuario.db')
    c = conn.cursor()    
    sql = 'SELECT EXISTS(SELECT * FROM usuario WHERE email = "{}")'.format(mail)
    c.execute(sql)
    temp = c.fetchone()
    for v in temp:
        if v == 1:
            return True
        else:
            return False
#Funcao para verificar se a tabela existe ou nao e criar    
def createTable(): 
    conn = sqlite3.connect('usuario.db')
    c = conn.cursor()
    texto = ''
    sql = """CREATE TABLE IF NOT EXISTS usuario (
        id integer PRIMARY KEY,
        name text NOT NULL,
        email text NOT NULL,
        password text NOT NULL,
        phone text NOT NULL);"""
    try:
        c.execute(sql)
        conn.commit()
    except:
        print(sys.exc_info())
        texto = 'Problemas para Criar a tabela'
    conn.close()
    msg = { 'messagem': texto}
    return msg
