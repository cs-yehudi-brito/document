#-*- coding: utf-8 -*-
import sqlite3
import json
import sys
def delete(id):
    print(id)
    conn = sqlite3.connect('usuario.db')
    c = conn.cursor()
    texto = ''
    c.execute("""DELETE FROM usuario WHERE id = ? """, (id))
    try:
        conn.commit()
        texto = 'Registro excluído com sucesso'
        #msg = { 'messagem': texto}
        #return msg
    except:
        print(sys.exc_info())
        texto = 'Ruim'
    conn.close()
    msg = { 'messagem': texto}
    return msg