#-*- coding:utf-8 -*-
import sqlite3
import sys

def update(id, data):
    conn = sqlite3.connect('usuario.db')
    c = conn.cursor()
    phone = ''
    texto = ''
    for key, value in data.items():
        if key == "name":
            p_name = value
        elif key == "email":
            p_email = value
        elif key == "password":
            p_password = value
        elif key == "phones":
            for v in value:
                phone = v['ddd']+' '+v['number']
            p_phone = phone
    c.execute("""
    UPDATE usuario SET name = ?, email = ?, password = ?, 
    phone = ? WHERE id = ?
    """,(p_name, p_email, p_password, p_phone, id))
    try:
        conn.commit()
        texto = 'Dados alterados com sucesso'
    except:
        print(sys.exc_info())
        texto = 'Problemas para fazer as alterações'
    conn.close()
    msg = { 'messagem': texto}
    return msg        